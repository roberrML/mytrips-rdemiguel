package com.rober.mytrips.view.mainLogin.activity;

import com.rober.mytrips.interactor.login.LoginInteractor;
import com.rober.mytrips.view.mainLogin.fragment.MainLoginPresenterImp;

import javax.inject.Inject;

/**
 * Created by roberto.demiguel on 11/01/2017.
 */
public class MainLoginActivityPresenterImp implements MainLoginActivityContract.Presenter {

    private MainLoginActivityContract.View view;
    private LoginInteractor interactor;

    @Inject
    public MainLoginActivityPresenterImp(MainLoginActivityContract.View view, LoginInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void checkLogin() {
        interactor.checkLogin(new MainLoginPresenterImp.LoginCheckListener() {
            @Override
            public void onPreviousLogin() {
                view.goToMainActivity();
            }

            @Override
            public void onNotPreviusLogin() {
                view.goToLoginActivity();
            }
        });
    }
}
