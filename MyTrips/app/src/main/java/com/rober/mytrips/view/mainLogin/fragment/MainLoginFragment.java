package com.rober.mytrips.view.mainLogin.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.rober.mytrips.MainApplication;
import com.rober.mytrips.R;
import com.rober.mytrips.view.emailLogin.EmailLoginActivity;
import com.rober.mytrips.view.main.MainActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.facebook.FacebookSdk.getApplicationContext;


public class MainLoginFragment extends Fragment implements MainLoginContract.View {

    private static final String TAG = "MainLoginFragment";
    private static final int RC_SIGN_IN = 12345;

    @Inject
    MainLoginContract.Presenter presenter;
    @Inject
    GoogleApiClient googleApiClient;
    @Inject
    CallbackManager facebookCallbackManager;
    @Inject
    ProgressDialog dialog;

    @BindView(R.id.login_facebook_button)
    LoginButton loginButton;

    private GooglePlayServicesCallback googlePlayServicesCallback;

    public MainLoginFragment() {
    }

    public static MainLoginFragment newInstance() {
        return new MainLoginFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((MainApplication) getActivity().getApplication()).getAppComponent().plus(new MainLoginFragmentModule(this, getActivity())).inject(this);

        dialog = new ProgressDialog(getActivity());

    }

    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container,
                                          Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        android.view.View view = inflater.inflate(R.layout.fragment_main_login, container, false);
        ButterKnife.bind(this, view);

        initFacebookLoginButton();

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            presenter.handleSignInResult(result);
        } else {
            facebookCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void initFacebookLoginButton() {
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.setFragment(this);

        loginButton.registerCallback(facebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onPhotoSuccessfullySaved:" + loginResult);
                presenter.handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });
    }


    @OnClick(R.id.login_email_button)
    public void loginWithMail() {
        startActivity(new Intent(getApplicationContext(), EmailLoginActivity.class));
    }


    @OnClick(R.id.login_google_button)
    public void loginWithGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    public static class GoogleApiClientOnConnectionFailedListener implements GoogleApiClient.OnConnectionFailedListener {

        private Context context;

        GoogleApiClientOnConnectionFailedListener(Context context) {
            this.context = context;
        }

        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            Toast.makeText(context, R.string.google_login_connection_failed, Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof GooglePlayServicesCallback) {
            googlePlayServicesCallback = (GooglePlayServicesCallback) context;
        } else {
            throw new RuntimeException(context.toString());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        googlePlayServicesCallback = null;
    }

    @Override
    public void showProgress() {
        dialog.setMessage(getString(R.string.loading_dialog));
        dialog.show();
    }

    @Override
    public void hideProgress() {
        dialog.dismiss();
    }

    @Override
    public void showGooglePlayServicesDialog(int codeError) {
        googlePlayServicesCallback.onInvokeGooglePlayServices(codeError);
    }

    @Override
    public void showGooglePlayServicesError() {
        Toast.makeText(getActivity(), R.string.google_play_services_required, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNetworkError() {
        Toast.makeText(getActivity(), R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void goToMainActivity() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        getActivity().finish();
    }

    @Override
    public void showLoginError(String msg) {
        if (msg != null) {
            Log.d(TAG, msg);
        }
        Toast.makeText(getActivity(), R.string.google_authentication_failed, Toast.LENGTH_SHORT).show();

    }

    public interface GooglePlayServicesCallback {
        void onInvokeGooglePlayServices(int codeError);
    }

}
