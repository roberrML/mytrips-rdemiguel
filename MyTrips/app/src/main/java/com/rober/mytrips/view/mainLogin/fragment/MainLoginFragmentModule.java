package com.rober.mytrips.view.mainLogin.fragment;


import android.app.ProgressDialog;
import android.support.v4.app.FragmentActivity;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.rober.mytrips.R;
import com.rober.mytrips.dagger.scopes.ActivityScope;
import com.rober.mytrips.interactor.login.LoginInteractor;
import com.rober.mytrips.interactor.login.imp.LoginInteractorImp;

import dagger.Module;
import dagger.Provides;

@Module
public class MainLoginFragmentModule {

    public final MainLoginContract.View view;
    public final FragmentActivity context;

    public MainLoginFragmentModule(MainLoginContract.View view, FragmentActivity context) {
        this.view = view;
        this.context = context;
    }

    @Provides
    @ActivityScope
    MainLoginContract.View provideMainLoginView() {
        return this.view;
    }

    @Provides
    @ActivityScope
    LoginInteractor provideLoginInteractor(LoginInteractorImp interactor) {
        return interactor;
    }

    @Provides
    @ActivityScope
    MainLoginContract.Presenter provideMainLoginPresenter(MainLoginPresenterImp presenter) {
        return presenter;
    }

    @Provides
    @ActivityScope
    GoogleApiClient provideGoogleApiClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(context.getString(R.string.oauth_web_client_id)).requestEmail().build();
        // Build a GoogleApiClient with access to the Google Sign-In API and the options specified by gso.
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context).enableAutoManage(context, new MainLoginFragment.GoogleApiClientOnConnectionFailedListener(context)).addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();

        return googleApiClient;
    }

    @Provides
    @ActivityScope
    CallbackManager provideFacebookCallbackManager() {
        FacebookSdk.sdkInitialize(context);
        return CallbackManager.Factory.create();
    }

    @Provides
    @ActivityScope
    ProgressDialog provideProgressDialog() {
        return new ProgressDialog(context);
    }


}
