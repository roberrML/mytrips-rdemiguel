package com.rober.mytrips.view.emailLogin;


import com.rober.mytrips.dagger.scopes.ActivityScope;
import com.rober.mytrips.interactor.login.LoginInteractor;
import com.rober.mytrips.interactor.login.imp.LoginInteractorImp;

import dagger.Module;
import dagger.Provides;

@Module
public class EmailLoginFragmentModule {

    public final EmailLoginContract.View view;

    public EmailLoginFragmentModule(EmailLoginContract.View view) {
        this.view = view;
    }

    @Provides
    @ActivityScope
    EmailLoginContract.View provideMainView() {
        return this.view;
    }

    @Provides
    @ActivityScope
    LoginInteractor provideMainInteractor(LoginInteractorImp interactor) {
        return interactor;
    }

    @Provides
    @ActivityScope
    EmailLoginContract.Presenter provideMainPresenter(EmailLoginPresenterImp presenter) {
        return presenter;
    }
}
