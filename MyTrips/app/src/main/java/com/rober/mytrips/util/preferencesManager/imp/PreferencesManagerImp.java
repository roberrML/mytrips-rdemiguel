package com.rober.mytrips.util.preferencesManager.imp;

import android.content.Context;
import android.content.SharedPreferences;

import com.rober.mytrips.util.preferencesManager.PreferencesManager;

import java.util.Calendar;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by roberto.demiguel on 14/10/2016.
 */
public class PreferencesManagerImp implements PreferencesManager {

    private static final String PREFERENCES_NAME = "MyPreferences";
    private static final String LAST_LOAD_FROM_SERVER = "LAST_LOAD_FROM_SERVER";

    private final SharedPreferences prefs;

    @Inject
    public PreferencesManagerImp(Context context) {
        prefs = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public void saveStringPreference(String key, String value) {
        prefs.edit().putString(key, value).apply();
    }

    @Override
    public void saveStringPreferences(Map<String, String> preferences) {
        SharedPreferences.Editor editor = prefs.edit();
        for (Map.Entry<String, String> entry : preferences.entrySet()) {
            editor.putString(entry.getKey(), entry.getValue());
        }
        editor.apply();
    }

    @Override
    public String retrieveStringPreference(String key) {
        return prefs.getString(key, null);
    }

    @Override
    public void saveLastLoadFromServerPreference() {
        prefs.edit().putLong(LAST_LOAD_FROM_SERVER, Calendar.getInstance().getTimeInMillis()).apply();
    }

    @Override
    public Long retrieveLastLoadFromServerPreference() {
        Long result = prefs.getLong(LAST_LOAD_FROM_SERVER, -1);
        if (result == -1) {
            result = null;
        }
        return result;
    }
}
