package com.rober.mytrips.data.googlePlacePhoto;

/**
 * Created by roberto.demiguel on 25/01/2017.
 */
public interface ObtainPhotoResultCallback {

    void onPhotoSuccessfullySaved(String imageSavedPath);

    void onPhotoNotSaved();
}
