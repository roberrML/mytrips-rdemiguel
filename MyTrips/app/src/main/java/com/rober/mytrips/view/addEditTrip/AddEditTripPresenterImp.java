package com.rober.mytrips.view.addEditTrip;

import com.google.android.gms.common.api.GoogleApiClient;
import com.rober.mytrips.interactor.trip.TripsInteractor;
import com.rober.mytrips.interactor.trip.imp.TripsInteractorImp;
import com.rober.mytrips.model.Trip;

import javax.inject.Inject;


/**
 * Created by roberto.demiguel on 05/01/2017.
 */
public class AddEditTripPresenterImp implements AddEditTripContract.Presenter {

    private AddEditTripContract.View view;
    private TripsInteractor interactor;
    private GoogleApiClient googleApiClient;

    @Inject
    public AddEditTripPresenterImp(AddEditTripContract.View view, TripsInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void setGoogleApiClient(GoogleApiClient googleApiClient) {
        this.googleApiClient = googleApiClient;
    }

    @Override
    public void saveTrip(final Trip trip) {
        if (view != null) {
            view.showProgress();
        }
        //we set googleApiClient in interactor in order to get google place photo before persisting the trip
        if (interactor instanceof TripsInteractorImp) {
            ((TripsInteractorImp) interactor).setGoogleApiClient(googleApiClient);
        }
        //create another call if we want to make a difference in UI between update or save operation.
        interactor.saveTrip(trip, new TripSaveCallback());
    }

    @Override
    public void updateTrip(Trip trip) {
        if (view != null) {
            view.showProgress();
        }
        //we set googleApiClient in interactor in order to get google place photo before persisting the trip
        if (interactor instanceof TripsInteractorImp) {
            ((TripsInteractorImp) interactor).setGoogleApiClient(googleApiClient);
        }
        //create another call if we want to make a difference in UI between update or save operation.
        interactor.updateTrip(trip, new TripSaveCallback());
    }

    private class TripSaveCallback implements TripsInteractor.TripRequestCallback {

        public TripSaveCallback() {
        }

        @Override
        public void onRequestSuccess() {
            if (view != null) {
                view.hideProgress();
                view.showTripsScreenOnSuccess();
            }
        }

        @Override
        public void onRequestFailed() {
            if (view != null) {
                view.hideProgress();
                view.showTripsScreenOnError();
            }
        }
    }

    @Override
    public void loadTrip(String tripId) {
        if (view != null) {
            view.showProgress();
        }
        interactor.getTripById(tripId, new TripsInteractor.GetTripCallback() {
            @Override
            public void onTripLoaded(Trip trip) {
                if (view != null) {
                    view.populateTrip(trip);
                    view.hideProgress();
                }
            }

            @Override
            public void onDataNotAvailable() {
                if (view != null) {
                    view.showLoadTripError();
                    view.hideProgress();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }
}
