package com.rober.mytrips.interactor.main;

import com.rober.mytrips.interactor.RequestCallback;
import com.rober.mytrips.model.User;

/**
 * Created by roberto.demiguel on 11/01/2017.
 */
public interface MainInteractor {

    void closeSession(final RequestCallback<Object> callback);

    void getUserInfo(final RequestCallback<User> callback);
}
