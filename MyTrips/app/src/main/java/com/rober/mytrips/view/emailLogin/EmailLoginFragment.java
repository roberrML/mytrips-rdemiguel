package com.rober.mytrips.view.emailLogin;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.rober.mytrips.MainApplication;
import com.rober.mytrips.R;
import com.rober.mytrips.util.FormValidatorUtil;
import com.rober.mytrips.view.emailRememberPassword.EmailRememberPasswordActivity;
import com.rober.mytrips.view.emailSignup.EmailSignupActivity;
import com.rober.mytrips.view.main.MainActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by roberto.demiguel on 13/12/2016.
 */
public class EmailLoginFragment extends Fragment implements EmailLoginContract.View {

    @Inject
    EmailLoginContract.Presenter presenter;

    @BindView(R.id.login_email_field)
    EditText emailField;
    @BindView(R.id.login_password_field)
    EditText passwordField;
    @BindView(R.id.login_email_layout)
    TextInputLayout emailLayout;
    @BindView(R.id.login_password_layout)
    TextInputLayout passwordLayout;
    @BindView(R.id.login_progress_bar)
    ProgressBar progressBar;

    public EmailLoginFragment() {
    }

    public static EmailLoginFragment newInstance() {
        return new EmailLoginFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((MainApplication) getActivity().getApplication()).getAppComponent().plus(new EmailLoginFragmentModule(this)).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_email_login, container, false);

        ButterKnife.bind(this, view);
        initEvents();

        return view;
    }

    private void initEvents() {
        emailField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                emailLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        passwordField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                passwordLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case EmailLoginActivity.REQUEST_GOOGLE_PLAY_SERVICES:
                attemptLogin();
                break;
        }
    }

    @OnClick(R.id.btn_remember_password)
    public void goToRememberPasswordActivity() {
        startActivity(new Intent(getApplicationContext(), EmailRememberPasswordActivity.class));
    }

    @OnClick(R.id.btn_signup)
    public void goToSignupActivity() {
        startActivity(new Intent(getApplicationContext(), EmailSignupActivity.class));
    }


    @OnClick(R.id.btn_login)
    public void attemptLogin() {
        if (inputIsValid()) {
            presenter.attemptLogin(emailField.getText().toString(), passwordField.getText().toString());
        }
    }

    @Override
    public void showEmailNotVerifiedMessage() {
        AlertDialog alertDialog = createEmailNotVerifiedAlertDialog();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(android.R.color.black));
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(android.R.color.black));
    }

    private AlertDialog createEmailNotVerifiedAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setTitle(R.string.email_not_verified_title);
        dialogBuilder.setMessage(R.string.email_not_verified_msg);

        dialogBuilder.setNegativeButton(R.string.send_verification_email, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.sendVerificationEmail();
            }
        });
        dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        return dialogBuilder.create();
    }


    private boolean inputIsValid() {
        return FormValidatorUtil.validateEmail(getActivity(), emailField, emailLayout)
                && FormValidatorUtil.validatePassword(getActivity(), passwordField, passwordLayout);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void goToMainActivity() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void showLoginError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNetworkError() {
        Toast.makeText(getActivity(), R.string.no_internet_connection, Toast.LENGTH_LONG).show();
    }
}
