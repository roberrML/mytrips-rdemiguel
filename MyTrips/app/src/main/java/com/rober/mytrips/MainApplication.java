package com.rober.mytrips;

import android.app.Application;

import com.rober.mytrips.dagger.AppComponent;
import com.rober.mytrips.dagger.DaggerAppComponent;
import com.rober.mytrips.dagger.modules.AppModule;

/**
 * Created by roberto.demiguel on 13/12/2016.
 */
public class MainApplication extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
