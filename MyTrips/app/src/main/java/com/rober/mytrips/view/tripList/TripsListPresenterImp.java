package com.rober.mytrips.view.tripList;

import com.google.android.gms.common.api.GoogleApiClient;
import com.rober.mytrips.interactor.trip.TripsInteractor;
import com.rober.mytrips.interactor.trip.imp.TripsInteractorImp;
import com.rober.mytrips.model.Trip;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by roberto.demiguel on 05/01/2017.
 */

public class TripsListPresenterImp implements TripsListContract.Presenter {

    private TripsListContract.View view;
    private TripsInteractor interactor;
    private GoogleApiClient googleApiClient;

    @Inject
    TripsListPresenterImp(TripsListContract.View view, TripsInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void loadTrips() {
        if (view != null) {
            view.showProgress();
        }
        //we set googleApiClient in interactor in order to get google place photo before persisting the trip
        if (interactor instanceof TripsInteractorImp) {
            ((TripsInteractorImp) interactor).setGoogleApiClient(googleApiClient);
        }

        interactor.getTrips(new TripsInteractor.LoadTripsCallback() {
            @Override
            public void onTripsLoaded(List<Trip> trips) {
                if (view != null) {
                    view.populateTrips(trips);
                    view.hideProgress();
                }
            }

            @Override
            public void onDataNotAvailable() {
                if (view != null) {
                    view.showLoadTripsError();
                    view.hideProgress();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void setGoogleApiClient(GoogleApiClient googleApiClient) {
        this.googleApiClient = googleApiClient;
    }
}
