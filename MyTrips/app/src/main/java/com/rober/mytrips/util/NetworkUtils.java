package com.rober.mytrips.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

/**
 * Created by roberto.demiguel on 22/12/2016.
 */
public class NetworkUtils {

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public static boolean isGooglePlayServicesAvailable(Context context, PlayServicesCheckListener listener) {
        boolean result = true;
        int statusCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
        if (GoogleApiAvailability.getInstance().isUserResolvableError(statusCode)) {
            listener.onBeUserResolvableError(statusCode);
            result = false;
        } else if (statusCode != ConnectionResult.SUCCESS) {
            listener.onGooglePlayServicesFailed();
            result = false;
        }
        return result;
    }
}
