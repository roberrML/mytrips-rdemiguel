package com.rober.mytrips.view.addEditTrip;

import com.google.android.gms.common.api.GoogleApiClient;
import com.rober.mytrips.model.Trip;

/**
 * Created by roberto.demiguel on 19/12/2016.
 */
public class AddEditTripContract {

    interface View {

        void showProgress();

        void hideProgress();

        void populateTrip(Trip trip);

        void showLoadTripError();

        void showTripsScreenOnSuccess();

        void showTripsScreenOnError();
    }

    interface Presenter {

        void setGoogleApiClient(GoogleApiClient googleApiClient);

        void saveTrip(Trip trip);

        void updateTrip(Trip trip);

        void loadTrip(String tripId);

        void onDestroy();
    }


}
