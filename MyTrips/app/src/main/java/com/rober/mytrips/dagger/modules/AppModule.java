package com.rober.mytrips.dagger.modules;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.rober.mytrips.MainApplication;
import com.rober.mytrips.util.preferencesManager.PreferencesManager;
import com.rober.mytrips.util.preferencesManager.imp.PreferencesManagerImp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by roberto.demiguel on 13/12/2016.
 */
@Module
public class AppModule {

    private MainApplication mainApplication;

    public AppModule(MainApplication mainApplication) {
        this.mainApplication = mainApplication;
    }

    @Provides
    @Singleton
    MainApplication provideMainApplication() {
        return mainApplication;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(MainApplication mainApplication) {
        return PreferenceManager.getDefaultSharedPreferences(mainApplication);
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mainApplication;
    }

    @Provides
    @Singleton
    String providePackageName() {
        return mainApplication.getPackageName();
    }

    @Provides
    @Singleton
    PreferencesManager providePreferencesManager(PreferencesManagerImp preferencesManagerImp) {
        return preferencesManagerImp;
    }
}
