package com.rober.mytrips.data.googlePlacePhoto;

import android.support.annotation.NonNull;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.Places;


/**
 * Created by roberto.demiguel on 24/01/2017.
 */
public class ImageGetterFromGooglePlacesUtil {

    /**
     * Get a place photo from Google Places Api
     *
     * @param placeId         - where we want to obtain a photo
     * @param googleApiClient - needed to consume the service
     * @param callback        - where result will be obtained
     */
    public static void getPhotoFromGooglePlacesApi(String placeId, final GoogleApiClient googleApiClient, final PlacePhotoResultCallback callback) {
        Places.GeoDataApi.getPlacePhotos(googleApiClient, placeId).setResultCallback(new ResultCallback<PlacePhotoMetadataResult>() {
            @Override
            public void onResult(@NonNull PlacePhotoMetadataResult photos) {
                if (photos.getStatus().isSuccess()) {
                    PlacePhotoMetadataBuffer photoMetadataBuffer = photos.getPhotoMetadata();
                    if (photoMetadataBuffer.getCount() > 0) {
                        // Get the first bitmap
                        photoMetadataBuffer.get(0).getPhoto(googleApiClient).setResultCallback(callback);
                    } else {
                        callback.onFailure();
                    }
                    photoMetadataBuffer.release();
                } else {
                    callback.onFailure();
                }

            }
        });

    }

}
