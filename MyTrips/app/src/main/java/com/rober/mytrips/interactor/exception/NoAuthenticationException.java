package com.rober.mytrips.interactor.exception;

/**
 * Created by roberto.demiguel on 09/02/2017.
 */
public class NoAuthenticationException extends Exception {

    public NoAuthenticationException() {
        super();
    }
}
