package com.rober.mytrips.util;

/**
 * Created by roberto.demiguel on 04/01/2017.
 */
public interface PlayServicesCheckListener {

    void onBeUserResolvableError(int errorCode);

    void onGooglePlayServicesFailed();
}
