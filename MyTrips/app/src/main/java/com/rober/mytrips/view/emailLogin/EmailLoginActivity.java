package com.rober.mytrips.view.emailLogin;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.rober.mytrips.R;

/**
 * Created by roberto.demiguel on 13/12/2016.
 */
public class EmailLoginActivity extends AppCompatActivity {

    public static final int REQUEST_GOOGLE_PLAY_SERVICES = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_login);

        EmailLoginFragment fragment = (EmailLoginFragment) getSupportFragmentManager().findFragmentById(R.id.activity_login);
        if (fragment == null) {
            fragment = EmailLoginFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.activity_login, fragment).commit();
        }
    }

}

