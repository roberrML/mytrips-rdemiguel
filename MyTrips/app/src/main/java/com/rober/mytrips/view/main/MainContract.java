package com.rober.mytrips.view.main;

import com.rober.mytrips.model.User;

/**
 * Created by roberto.demiguel on 10/01/2016.
 */
public class MainContract {

    interface View {

        void showRequestError(String msg);

        void showNetworkError();

        void populateUserInfo(User user);

        void goToMainLoginActivity();

    }

    interface Presenter {

        void getUserInfo();

        void closeSession();

        void onDestroy();

    }


}
