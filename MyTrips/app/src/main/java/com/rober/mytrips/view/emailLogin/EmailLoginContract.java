package com.rober.mytrips.view.emailLogin;

/**
 * Created by roberto.demiguel on 19/12/2016.
 */
public class EmailLoginContract {

    interface View {

        void showProgress();

        void hideProgress();

        void goToMainActivity();

        void showLoginError(String msg);

        void showNetworkError();

        void showEmailNotVerifiedMessage();
    }

    interface Presenter {

        void attemptLogin(String email, String password);

        void onDestroy();

        void sendVerificationEmail();
    }


}
