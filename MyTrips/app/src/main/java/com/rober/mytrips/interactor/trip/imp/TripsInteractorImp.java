package com.rober.mytrips.interactor.trip.imp;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.api.GoogleApiClient;
import com.rober.mytrips.data.ImageStorageUtil;
import com.rober.mytrips.data.googlePlacePhoto.ImageGetterFromGooglePlacesUtil;
import com.rober.mytrips.data.googlePlacePhoto.ObtainPhotoResultCallback;
import com.rober.mytrips.data.googlePlacePhoto.PlacePhotoResultCallbackImp;
import com.rober.mytrips.interactor.trip.GoogleApiClientInteractor;
import com.rober.mytrips.interactor.trip.Local;
import com.rober.mytrips.interactor.trip.Remote;
import com.rober.mytrips.interactor.trip.TripsInteractor;
import com.rober.mytrips.model.Trip;
import com.rober.mytrips.util.preferencesManager.PreferencesManager;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by roberto.demiguel on 10/01/2017.
 */
@Singleton
public class TripsInteractorImp implements TripsInteractor, GoogleApiClientInteractor {

    //3 days
    private static final long refreshTimeInMillis = 3 * 24 * 60 * 60 * 1000;

    private TripsInteractor tripsInteractorRemote;
    private TripsInteractor tripsInteractorLocal;
    private String packageName;
    private PreferencesManager preferencesManager;
    private GoogleApiClient googleApiClient;

    private Map<String, Trip> cachedTrips;

    @Inject
    public TripsInteractorImp(@Remote TripsInteractor tripsInteractorRemote, @Local TripsInteractor tripsInteractorLocal, String packageName, PreferencesManager preferencesManager) {
        this.tripsInteractorRemote = tripsInteractorRemote;
        this.tripsInteractorLocal = tripsInteractorLocal;
        this.packageName = packageName;
        this.preferencesManager = preferencesManager;
    }


    @Override
    public void getTrips(@NonNull final LoadTripsCallback callback) {
        if (cachedTrips != null) {
            callback.onTripsLoaded(new ArrayList<>(cachedTrips.values()));
        } else {
            if (isNeededToRefreshFromRemote()) {
                getTripsFromRemoteDataSource(callback);
            } else {
                getTripsFromLocalDataSource(callback);
            }
        }
    }

    private void getTripsFromRemoteDataSource(@NonNull final LoadTripsCallback callback) {
        tripsInteractorRemote.getTrips(new LoadTripsCallback() {
            @Override
            public void onTripsLoaded(final List<Trip> trips) {
                preferencesManager.saveLastLoadFromServerPreference();
                refreshLocalTripImages(trips, new TripRefreshImagesCallback() {
                    @Override
                    public void onFinished() {
                        refreshCache(trips);
                        refreshLocalDataSource(trips);
                        callback.onTripsLoaded(new ArrayList<>(cachedTrips.values()));
                    }
                });
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    /**
     * Get images from Google Api Places for trips without stored image. This can happen when user
     * gets trips from remote, and images are not in storage
     *
     * @param trips
     */
    private void refreshLocalTripImages(List<Trip> trips, final TripRefreshImagesCallback refreshLocalTripImagesCallback) {
        if (trips.isEmpty()) {
            refreshLocalTripImagesCallback.onFinished();
        }
        for (Trip trip : trips) {
            if (trip.getImagePath() != null) {
                File tripCover = new File(trip.getImagePath());
                if (!tripCover.exists()) {
                    obtainPhotoPlaceAndPersist(trip, TripOperation.REFRESH_LOCAL_IMAGE, new TripRequestCallback() {
                        @Override
                        public void onRequestSuccess() {
                            refreshLocalTripImagesCallback.onFinished();
                        }

                        @Override
                        public void onRequestFailed() {
                            refreshLocalTripImagesCallback.onFinished();
                        }
                    });
                }
            }
        }
    }

    private void getTripsFromLocalDataSource(@NonNull final LoadTripsCallback callback) {
        tripsInteractorLocal.getTrips(new LoadTripsCallback() {
            @Override
            public void onTripsLoaded(List<Trip> trips) {
                refreshCache(trips);
                callback.onTripsLoaded(new ArrayList<>(cachedTrips.values()));
            }

            @Override
            public void onDataNotAvailable() {
                getTripsFromRemoteDataSource(callback);
            }
        });
    }


    @Override
    public void getTripById(@NonNull final String tripId, @NonNull final GetTripCallback callback) {
        Trip cachedTrip = getTripFromCache(tripId);
        if (cachedTrip != null) {
            callback.onTripLoaded(cachedTrip);
        } else {
            getTripFromDataSource(tripId, callback);
        }
    }

    private void getTripFromDataSource(@NonNull String tripId, @NonNull GetTripCallback callback) {
        if (isNeededToRefreshFromRemote()) {
            getTripByIdFromRemoteDataSource(tripId, callback);
        } else {
            getTripByIdFromLocalDataSource(tripId, callback);
        }
    }

    private boolean isNeededToRefreshFromRemote() {
        Long lastLoad = preferencesManager.retrieveLastLoadFromServerPreference();
        return lastLoad == null || timeIsLongerThanAWeek(lastLoad);
    }

    private void getTripByIdFromRemoteDataSource(@NonNull final String tripId, @NonNull final GetTripCallback callback) {
        tripsInteractorRemote.getTripById(tripId, new GetTripCallback() {
            @Override
            public void onTripLoaded(Trip trip) {
                callback.onTripLoaded(trip);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    private void getTripByIdFromLocalDataSource(@NonNull final String tripId, @NonNull final GetTripCallback callback) {
        tripsInteractorLocal.getTripById(tripId, new GetTripCallback() {
            @Override
            public void onTripLoaded(Trip trip) {
                callback.onTripLoaded(trip);
            }

            @Override
            public void onDataNotAvailable() {
                getTripByIdFromRemoteDataSource(tripId, callback);
            }
        });
    }

    @Override
    public void saveTrip(@NonNull final Trip trip, final TripRequestCallback tripRequestCallback) {
        obtainPhotoPlaceAndPersist(trip, TripOperation.SAVE, tripRequestCallback);
    }

    @Override
    public void updateTrip(@NonNull Trip trip, @NonNull TripRequestCallback tripRequestCallback) {
        obtainPhotoPlaceAndPersist(trip, TripOperation.UPDATE, tripRequestCallback);
    }

    private void obtainPhotoPlaceAndPersist(@NonNull final Trip trip, final TripOperation tripOperation, final TripRequestCallback tripRequestCallback) {
        //if for any reason googleApiClient is null, we must persistTrip anyway; other case, we'll get cover before persisting
        if ((googleApiClient == null || trip.getGooglePlaceId() == null) && !tripOperation.equals(TripOperation.REFRESH_LOCAL_IMAGE)) {
            persistTrip(trip, tripRequestCallback, tripOperation);
        } else if (googleApiClient != null && trip.getGooglePlaceId() != null) {
            ImageGetterFromGooglePlacesUtil.getPhotoFromGooglePlacesApi(trip.getGooglePlaceId(), googleApiClient,
                    new PlacePhotoResultCallbackImp(trip.getGooglePlaceId(), packageName, new ObtainPhotoResultCallback() {
                        @Override
                        public void onPhotoSuccessfullySaved(String imageSavedPath) {
                            trip.setImagePath(imageSavedPath);
                            if (!tripOperation.equals(TripOperation.REFRESH_LOCAL_IMAGE)) {
                                persistTrip(trip, tripRequestCallback, tripOperation);
                            } else {
                                tripRequestCallback.onRequestSuccess();
                            }
                        }

                        @Override
                        public void onPhotoNotSaved() {
                            if (!tripOperation.equals(TripOperation.REFRESH_LOCAL_IMAGE)) {
                                persistTrip(trip, tripRequestCallback, tripOperation);
                            } else {
                                tripRequestCallback.onRequestFailed();
                            }
                        }
                    }));
        }
    }


    private void persistTrip(@NonNull Trip trip, TripRequestCallback tripRequestCallback, TripOperation tripOperation) {
        TripRequestCallback persistCallback = new TripOperationCallback(trip, tripRequestCallback, tripOperation);
        if (tripOperation.equals(TripOperation.SAVE)) {
            tripsInteractorRemote.saveTrip(trip, persistCallback);
            tripsInteractorLocal.saveTrip(trip, persistCallback);
        } else if (tripOperation.equals(TripOperation.UPDATE)) {
            tripsInteractorRemote.updateTrip(trip, persistCallback);
            tripsInteractorLocal.updateTrip(trip, persistCallback);
        }
    }

    private void saveTripInCache(@NonNull Trip trip) {
        if (cachedTrips == null) {
            cachedTrips = new LinkedHashMap<>();
        }
        cachedTrips.put(trip.getId(), trip);
    }

    private class TripOperationCallback implements TripRequestCallback {

        private boolean firstExecution = false;
        private Trip trip;
        private TripRequestCallback tripRequestCallback;
        private TripOperation tripOperation;

        public TripOperationCallback(Trip trip, TripRequestCallback tripRequestCallback, TripOperation tripOperation) {
            this.trip = trip;
            this.tripRequestCallback = tripRequestCallback;
            this.tripOperation = tripOperation;
        }

        @Override
        public void onRequestSuccess() {
            if (!firstExecution) {
                firstExecution = true;
                if (tripOperation.equals(TripOperation.SAVE) || tripOperation.equals(TripOperation.UPDATE)) {
                    //save trip in cache only if we could save in remote or local
                    saveTripInCache(trip);
                } else if (tripOperation.equals(TripOperation.DELETE)) {
                    cachedTrips.remove(trip.getId());
                }
                tripRequestCallback.onRequestSuccess();
            }
        }

        @Override
        public void onRequestFailed() {
            if (!firstExecution) {
                firstExecution = true;
                tripRequestCallback.onRequestFailed();
            }
        }
    }


    @Override
    public void deleteTrip(@NonNull String tripId, @NonNull TripRequestCallback tripRequestCallback) {
        Trip trip = new Trip();
        trip.setId(tripId);
        TripRequestCallback persistCallback = new TripOperationCallback(trip, tripRequestCallback, TripOperation.DELETE);
        tripsInteractorRemote.deleteTrip(tripId, persistCallback);
        tripsInteractorLocal.deleteTrip(tripId, persistCallback);
    }

    @Override
    public void deleteAllTrips() {
        ImageStorageUtil.deleteAllTripImages(packageName);
        tripsInteractorLocal.deleteAllTrips();
        cachedTrips = null;
    }

    private void refreshCache(List<Trip> trips) {
        if (cachedTrips == null) {
            cachedTrips = new LinkedHashMap<>();
        }
        cachedTrips.clear();
        for (Trip trip : trips) {
            cachedTrips.put(trip.getId(), trip);
        }
    }

    private void refreshLocalDataSource(List<Trip> trips) {
        tripsInteractorLocal.deleteAllTrips();
        for (Trip trip : trips) {
            tripsInteractorLocal.saveTrip(trip, null);
        }
    }

    @Nullable
    private Trip getTripFromCache(@NonNull String id) {
        if (cachedTrips == null || cachedTrips.isEmpty()) {
            return null;
        } else {
            return cachedTrips.get(id);
        }
    }

    @Override
    public void setGoogleApiClient(GoogleApiClient googleApiClient) {
        this.googleApiClient = googleApiClient;
    }

    private enum TripOperation {
        SAVE, UPDATE, DELETE, REFRESH_LOCAL_IMAGE,
    }

    private boolean timeIsLongerThanAWeek(Long time) {
        boolean isLongerThanAWeek = false;
        if (Calendar.getInstance().getTimeInMillis() > (time + refreshTimeInMillis)) {
            isLongerThanAWeek = true;
        }
        return isLongerThanAWeek;
    }

}
