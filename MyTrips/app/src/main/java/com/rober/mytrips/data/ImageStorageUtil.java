package com.rober.mytrips.data;

import android.graphics.Bitmap;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.facebook.login.widget.ProfilePictureView.TAG;

/**
 * Created by roberto.demiguel on 24/01/2017.
 */
public class ImageStorageUtil {

    /**
     * Save image in internal storage
     *
     * @param image       - to save
     * @param packageName - of app in order to save it /Android/data/'packageName'/Files/'placeId'.png
     * @param placeId     - name of file
     * @return imagePath
     */
    public static String storeImage(Bitmap image, String packageName, String placeId) {
        String imagePath = null;
        try {
            File pictureFile = getOutputMediaFile(packageName, placeId);
            if (pictureFile == null) {
                Log.d(TAG, "Error creating media file, check storage permissions: ");// e.getMessage());
            } else {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                image.compress(Bitmap.CompressFormat.JPEG, 10, fos);
                imagePath = pictureFile.getPath();
                fos.flush();
                fos.close();
            }
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }

        return imagePath;
    }

    /**
     * Delete a file
     *
     * @param filePath - path of file to delete
     * @return true if and only if the file or directory is successfully deleted; false otherwise
     */
    public static boolean deleteFile(String filePath) {
        File fileToDelete = new File(filePath);
        return fileToDelete.delete();
    }

    /**
     * @param packageName - of app in order to save it /Android/data/'packageName'/Files/'placeId'.png
     * @param placeId     - name of file
     * @return mediaFile
     */
    private static File getOutputMediaFile(String packageName, String placeId) {
        File mediaStorageDir = getMediaStorageFolder(packageName);
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file with placeId as name to overwrite it if needed
        String fileName = placeId + ".png";
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + fileName);
        return mediaFile;
    }

    public static void deleteAllTripImages(String packageName){
        File mediaStorageDir = getMediaStorageFolder(packageName);
        String[] imageList = mediaStorageDir.list();
        for (String image : imageList) {
            new File(mediaStorageDir, image).delete();
        }
    }

    @NonNull
    private static File getMediaStorageFolder(String packageName) {
        return new File(Environment.getExternalStorageDirectory()
                    + "/Android/data/" + packageName + "/Files");
    }
}
