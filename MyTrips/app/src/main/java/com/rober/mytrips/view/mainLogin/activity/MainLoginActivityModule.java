package com.rober.mytrips.view.mainLogin.activity;

import com.rober.mytrips.dagger.scopes.ActivityScope;
import com.rober.mytrips.interactor.login.LoginInteractor;
import com.rober.mytrips.interactor.login.imp.LoginInteractorImp;

import dagger.Module;
import dagger.Provides;

/**
 * Created by roberto.demiguel on 11/01/2017.
 */
@Module
public class MainLoginActivityModule {

    public final MainLoginActivityContract.View view;

    public MainLoginActivityModule(MainLoginActivityContract.View view) {
        this.view = view;
    }

    @Provides
    @ActivityScope
    MainLoginActivityContract.View provideMainLoginView() {
        return this.view;
    }

    @Provides
    @ActivityScope
    LoginInteractor provideLoginInteractor(LoginInteractorImp interactor) {
        return interactor;
    }

    @Provides
    @ActivityScope
    MainLoginActivityContract.Presenter provideMainLoginPresenter(MainLoginActivityPresenterImp presenter) {
        return presenter;
    }
}
