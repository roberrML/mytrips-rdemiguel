package com.rober.mytrips.interactor;

/**
 * Created by roberto.demiguel on 04/01/2017.
 */

public interface RequestCallback<T> {

    void onRequestSuccess(T object);

    void onRequestFailed(String msg);
}
