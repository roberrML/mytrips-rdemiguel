package com.rober.mytrips.interactor.trip.imp.local;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.rober.mytrips.data.TripPersistenceContract;
import com.rober.mytrips.data.TripsDbHelper;
import com.rober.mytrips.interactor.trip.TripsInteractor;
import com.rober.mytrips.model.Trip;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by roberto.demiguel on 23/01/2017.
 */
@Singleton
public class TripsLocalInteractorImp implements TripsInteractor {

    private TripsDbHelper dbOpenHelper;
    private SQLiteDatabase db;

    @Inject
    public TripsLocalInteractorImp(TripsDbHelper dbOpenHelper) {
        this.dbOpenHelper = dbOpenHelper;
    }

    private void estabilishDb() {
        if (this.db == null) {
            this.db = this.dbOpenHelper.getWritableDatabase();
        }
    }

    @Override
    public void getTrips(@NonNull LoadTripsCallback callback) {
        List<Trip> trips = new ArrayList<>();

        estabilishDb();
        Cursor cursor = db.query(TripPersistenceContract.TripEntry.TABLE_NAME, null, null, null, null, null,
                TripPersistenceContract.TripEntry.START_DATE + " DESC");
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Trip trip = Trip.readTrip(cursor);
                trips.add(trip);
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        if (trips.isEmpty()) {
            // This will be called if the table is new or just empty.
            callback.onDataNotAvailable();
        } else {
            callback.onTripsLoaded(trips);
        }
    }

    @Override
    public void getTripById(@NonNull String tripId, @NonNull GetTripCallback callback) {
        estabilishDb();
        Trip trip = null;
        Cursor cursor = db.query(TripPersistenceContract.TripEntry.TABLE_NAME, null,
                TripPersistenceContract.TripEntry.ID + " LIKE ?", new String[]{tripId}, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            trip = Trip.readTrip(cursor);
        }
        if (cursor != null) {
            cursor.close();
        }
        if (trip != null) {
            callback.onTripLoaded(trip);
        } else {
            callback.onDataNotAvailable();
        }
    }

    @Override
    public void saveTrip(@NonNull Trip trip, TripRequestCallback tripRequestCallback) {
        estabilishDb();
        Long newId = db.insert(TripPersistenceContract.TripEntry.TABLE_NAME, null, trip.toContentValues());
        if (tripRequestCallback != null) {
            if (newId != -1) {
                tripRequestCallback.onRequestSuccess();
            } else {
                tripRequestCallback.onRequestFailed();
            }
        }
    }


    @Override
    public void updateTrip(@NonNull Trip trip, @NonNull TripRequestCallback tripRequestCallback) {
        estabilishDb();
        int rowsUpdated = db.update(TripPersistenceContract.TripEntry.TABLE_NAME, trip.toContentValues(),
                TripPersistenceContract.TripEntry.ID + " LIKE ?", new String[]{trip.getId()});
        if (rowsUpdated == 1) {
            tripRequestCallback.onRequestSuccess();
        } else {
            tripRequestCallback.onRequestFailed();
        }
    }


    @Override
    public void deleteTrip(@NonNull String tripId, @NonNull TripRequestCallback tripRequestCallback) {
        estabilishDb();
        int rowsDeleted = db.delete(TripPersistenceContract.TripEntry.TABLE_NAME,
                TripPersistenceContract.TripEntry.ID + " LIKE ?", new String[]{tripId});
        if (rowsDeleted == 1) {
            tripRequestCallback.onRequestSuccess();
        } else {
            tripRequestCallback.onRequestFailed();
        }
    }

    @Override
    public void deleteAllTrips() {
        estabilishDb();
        db.delete(TripPersistenceContract.TripEntry.TABLE_NAME, null, null);
    }
}
