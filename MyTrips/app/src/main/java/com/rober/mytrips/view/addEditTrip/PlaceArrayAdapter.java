package com.rober.mytrips.view.addEditTrip;


import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLngBounds;
import com.rober.mytrips.util.NetworkUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PlaceArrayAdapter extends ArrayAdapter<PlaceArrayAdapter.PlaceAutocomplete> implements Filterable {

    private static final String TAG = "PlaceArrayAdapter";
    private static final long PREDICTIONS_TIMEOUT = 60;

    private GoogleApiClient googleApiClient;
    private AutocompleteFilter autocompleteFilter;
    private LatLngBounds latLngBounds;
    private List<PlaceAutocomplete> placeAutocompleteList;

    public PlaceArrayAdapter(Context context, int resource, LatLngBounds bounds, AutocompleteFilter filter) {
        super(context, resource);
        latLngBounds = bounds;
        autocompleteFilter = filter;
    }

    public void setGoogleApiClient(GoogleApiClient googleApiClient) {
        if (googleApiClient == null || !googleApiClient.isConnected()) {
            this.googleApiClient = null;
        } else {
            this.googleApiClient = googleApiClient;
        }
    }

    @Override
    public int getCount() {
        return placeAutocompleteList.size();
    }

    @Override
    public PlaceAutocomplete getItem(int position) {
        return placeAutocompleteList.get(position);
    }

    private List<PlaceAutocomplete> getPredictions(CharSequence constraint) {
        List<PlaceAutocomplete> resultList = null;
        if (googleApiClient != null) {
            Log.i(TAG, "Executing autocomplete query for: " + constraint);
            PendingResult<AutocompletePredictionBuffer> results = Places.GeoDataApi.getAutocompletePredictions(googleApiClient, constraint.toString(), latLngBounds, autocompleteFilter);
            // Wait for predictions, set the timeout.
            AutocompletePredictionBuffer autocompletePredictions = results.await(PREDICTIONS_TIMEOUT, TimeUnit.SECONDS);
            final Status status = autocompletePredictions.getStatus();
            if (!status.isSuccess()) {
                showGettingPredictionsError(status);
            } else {
                resultList = getPlaceAutocompletes(autocompletePredictions);
            }
            autocompletePredictions.release();
        } else {
            Log.e(TAG, "Google API client is not connected.");
        }
        return resultList;
    }

    @NonNull
    private List<PlaceAutocomplete> getPlaceAutocompletes(AutocompletePredictionBuffer autocompletePredictions) {
        List<PlaceAutocomplete> resultList;
        Log.i(TAG, "Query completed. Received " + autocompletePredictions.getCount() + " predictions.");
        Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
        resultList = new ArrayList<>(autocompletePredictions.getCount());
        while (iterator.hasNext()) {
            AutocompletePrediction prediction = iterator.next();
            resultList.add(new PlaceAutocomplete(prediction.getPlaceId(), prediction.getFullText(null)));
        }
        return resultList;
    }

    private void showGettingPredictionsError(Status status) {
        Toast.makeText(getContext(), "Error: " + status.toString(), Toast.LENGTH_SHORT).show();
        Log.e(TAG, "Error getting place predictions: " + status.toString());
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint != null) {
                    // Query the autocomplete API for the entered constraint
                    if (NetworkUtils.isNetworkAvailable(getContext())) {
                        placeAutocompleteList = getPredictions(constraint);
                        if (placeAutocompleteList != null) {
                            results.values = placeAutocompleteList;
                            results.count = placeAutocompleteList.size();
                        }
                    }
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (apiReturnedResults(results)) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }

            private boolean apiReturnedResults(FilterResults results) {
                return results != null && results.count > 0;
            }
        };
    }

    class PlaceAutocomplete {
        private CharSequence placeId;
        private CharSequence description;

        PlaceAutocomplete(CharSequence placeId, CharSequence description) {
            this.placeId = placeId;
            this.description = description;
        }

        public CharSequence getPlaceId() {
            return placeId;
        }

        public void setPlaceId(CharSequence placeId) {
            this.placeId = placeId;
        }

        public CharSequence getDescription() {
            return description;
        }

        public void setDescription(CharSequence description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return description.toString();
        }
    }

}