package com.rober.mytrips.view.mainLogin.activity;

/**
 * Created by roberto.demiguel on 19/12/2016.
 */
public class MainLoginActivityContract {

    interface View {

        void goToMainActivity();

        void goToLoginActivity();
    }


    interface Presenter {

        void onDestroy();

        void checkLogin();
    }


}
