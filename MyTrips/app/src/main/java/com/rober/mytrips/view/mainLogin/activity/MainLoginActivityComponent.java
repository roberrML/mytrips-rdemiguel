package com.rober.mytrips.view.mainLogin.activity;


import com.rober.mytrips.dagger.scopes.ActivityScope;

import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = {MainLoginActivityModule.class})
public interface MainLoginActivityComponent {

    void inject(MainLoginActivity activity);
}

