package com.rober.mytrips.view.tripList;

import com.google.android.gms.common.api.GoogleApiClient;
import com.rober.mytrips.model.Trip;

import java.util.List;

/**
 * Created by roberto.demiguel on 19/12/2016.
 */
public class TripsListContract {

    interface View {

        void showProgress();

        void hideProgress();

        void populateTrips(List<Trip> tripList);

        void showLoadTripsError();

    }

    interface Presenter {

        void loadTrips();

        void onDestroy();

        void setGoogleApiClient(GoogleApiClient googleApiClient);
    }


}
