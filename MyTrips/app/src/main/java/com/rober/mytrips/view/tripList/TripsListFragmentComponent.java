package com.rober.mytrips.view.tripList;

import com.rober.mytrips.dagger.scopes.ActivityScope;

import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = {TripsListFragmentModule.class})
public interface TripsListFragmentComponent {

    void inject(TripsListFragment fragment);
}

