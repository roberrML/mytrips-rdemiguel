package com.rober.mytrips.interactor.trip;

import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by roberto.demiguel on 25/01/2017.
 */
public interface GoogleApiClientInteractor {

    void setGoogleApiClient(GoogleApiClient googleApiClient);
}
