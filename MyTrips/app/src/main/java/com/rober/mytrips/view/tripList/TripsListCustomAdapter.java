package com.rober.mytrips.view.tripList;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rober.mytrips.R;
import com.rober.mytrips.model.Trip;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Rober on 23/08/2016.
 */
public class TripsListCustomAdapter extends RecyclerView.Adapter<TripsListCustomAdapter.TripItemHolder> {

    private Fragment fragment;
    private List<Trip> trips;

    public TripsListCustomAdapter(Fragment fragment, List<Trip> trips) {
        this.fragment = fragment;
        this.trips = trips;
    }

    @Override
    public TripItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_trip, parent, false);
        return new TripItemHolder(v);
    }

    @Override
    public void onBindViewHolder(TripItemHolder holder, int position) {
        Trip trip = trips.get(position);
        holder.bind(trip, fragment);
    }

    @Override
    public int getItemCount() {
        return trips.size();
    }


    static class TripItemHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_name)
        TextView nameText;
        @BindView(R.id.item_date)
        TextView dateText;
        @BindView(R.id.item_location)
        TextView locationText;
        @BindView(R.id.item_cover)
        ImageView cover;

        TripItemHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        void bind(@NonNull Trip trip, @NonNull Fragment fragment) {
            nameText.setText(trip.getName());
            dateText.setText(Trip.dateFormatter.format(trip.getStartDate()));
            locationText.setText(trip.getLocation());
            if (trip.getImagePath() != null) {
                Glide.with(fragment.getActivity()).load(trip.getImagePath()).placeholder(R.drawable.default_cover).centerCrop().into(cover);
            } else {
                Glide.with(fragment.getActivity()).load(R.drawable.default_cover).centerCrop().into(cover);
            }
            itemView.setOnClickListener(new TripsListFragment.TripItemOnClickListener(trip, fragment));
        }
    }

}
