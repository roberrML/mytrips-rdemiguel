package com.rober.mytrips.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by roberto.demiguel on 22/12/2016.
 */
public class StringUtils {

    private final static String emailRegex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";

    public static boolean isEmailValid(String email) {
        Pattern pattern = Pattern.compile(emailRegex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String formatString(String text) {
        return (text == null) ? "" : text;
    }
}
