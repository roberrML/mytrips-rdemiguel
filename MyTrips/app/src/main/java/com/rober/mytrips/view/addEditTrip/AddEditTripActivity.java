package com.rober.mytrips.view.addEditTrip;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.rober.mytrips.R;
import com.rober.mytrips.view.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddEditTripActivity extends AppCompatActivity {

    public static final int REQUEST_ADD_TRIP = 1;

    @BindView(R.id.edit_toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_trip);

        ButterKnife.bind(this);

        initToolbar();

        String tripId = getIntent().getStringExtra(MainActivity.EXTRA_TRIP_ID);
        if (tripId == null) {
            setTitle(getString(R.string.add_trip));
        } else {
            setTitle(getString(R.string.edit_trip));
        }

        AddEditTripFragment addEditTripFragment = (AddEditTripFragment) getSupportFragmentManager().findFragmentById(R.id.add_edit_trip_container);
        if (addEditTripFragment == null) {
            addEditTripFragment = AddEditTripFragment.newInstance(tripId);
            getSupportFragmentManager().beginTransaction().add(R.id.add_edit_trip_container, addEditTripFragment).commit();
        }
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
