package com.rober.mytrips.data.googlePlacePhoto;

import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlacePhotoResult;

/**
 * Created by roberto.demiguel on 25/01/2017.
 */
public interface PlacePhotoResultCallback extends ResultCallback<PlacePhotoResult> {
    void onFailure();
}
