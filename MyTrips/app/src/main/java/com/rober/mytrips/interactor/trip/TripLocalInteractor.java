package com.rober.mytrips.interactor.trip;

/**
 * Created by roberto.demiguel on 25/01/2017.
 */
public interface TripLocalInteractor {

    void deleteAllTrips();
}
