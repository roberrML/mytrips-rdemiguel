package com.rober.mytrips.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Rober on 22/08/2016.
 */
public class TripsDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "MyTrips.db";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TripPersistenceContract.TripEntry.TABLE_NAME + " ("
                    + TripPersistenceContract.TripEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + TripPersistenceContract.TripEntry.ID + " TEXT NOT NULL,"
                    + TripPersistenceContract.TripEntry.NAME + " TEXT NOT NULL,"
                    + TripPersistenceContract.TripEntry.LOCATION + " TEXT NOT NULL,"
                    + TripPersistenceContract.TripEntry.GOOGLE_PLACE_ID + " TEXT,"
                    + TripPersistenceContract.TripEntry.LATITUDE + " DOUBLE,"
                    + TripPersistenceContract.TripEntry.LONGITUDE + " DOUBLE,"
                    + TripPersistenceContract.TripEntry.START_DATE + " DATE NOT NULL,"
                    + TripPersistenceContract.TripEntry.END_DATE + " DATE NOT NULL,"
                    + TripPersistenceContract.TripEntry.STAY_PLACE + " TEXT,"
                    + TripPersistenceContract.TripEntry.REASON + " TEXT,"
                    + TripPersistenceContract.TripEntry.COMPANIONS + " TEXT,"
                    + TripPersistenceContract.TripEntry.VISITED_PLACES + " TEXT,"
                    + TripPersistenceContract.TripEntry.VISITED_RESTAURANTS + " TEXT,"
                    + TripPersistenceContract.TripEntry.COMMENTS + " TEXT,"
                    + TripPersistenceContract.TripEntry.IMAGE_PATH + " TEXT,"
                    + "UNIQUE (" + TripPersistenceContract.TripEntry.ID + "))";

    public TripsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Not required as at version 1
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Not required as at version 1
    }
}
