package com.rober.mytrips.view.tripList;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.rober.mytrips.MainApplication;
import com.rober.mytrips.R;
import com.rober.mytrips.model.Trip;
import com.rober.mytrips.view.addEditTrip.AddEditTripActivity;
import com.rober.mytrips.view.main.MainActivity;
import com.rober.mytrips.view.tripDetail.TripDetailActivity;
import com.rober.mytrips.view.tripDetail.TripDetailFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TripsListFragment extends Fragment implements TripsListContract.View {

    @BindView(R.id.trips_list)
    RecyclerView tripsListView;
    @BindView(R.id.trips_list_empty_view)
    TextView emptyView;

    @Inject
    TripsListContract.Presenter presenter;
    @Inject
    ProgressDialog progressDialog;
    @Inject
    GoogleApiClient googleApiClient;

    private TripsListCustomAdapter tripsListCustomAdapter;
    private List<Trip> tripList;


    public TripsListFragment() {
    }

    public static TripsListFragment newInstance() {
        return new TripsListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MainApplication) getActivity().getApplication()).getAppComponent().plus(new TripsListFragmentModule(this, getActivity())).inject(this);
        tripList = new ArrayList<>();

        //we have to set googleApiClient in presenter in order to be able to get place photos from google api
        presenter.setGoogleApiClient(googleApiClient);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_trips_list, container, false);

        ButterKnife.bind(this, root);

        // Referencias UI
        tripsListCustomAdapter = new TripsListCustomAdapter(this, tripList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        tripsListView.setLayoutManager(linearLayoutManager);
        // Setup
        tripsListView.setAdapter(tripsListCustomAdapter);

        // Carga de datos
        presenter.loadTrips();

        return root;
    }


    @Override
    public void onPause() {
        super.onPause();
        googleApiClient.stopAutoManage(getActivity());
        googleApiClient.disconnect();
    }


    @OnClick(R.id.add_button)
    public void showAddScreenOnClick() {
        Intent intent = new Intent(getActivity(), AddEditTripActivity.class);
        startActivityForResult(intent, AddEditTripActivity.REQUEST_ADD_TRIP);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Activity.RESULT_OK == resultCode) {
            switch (requestCode) {
                case AddEditTripActivity.REQUEST_ADD_TRIP:
                    presenter.loadTrips();
                    break;
                case TripDetailFragment.REQUEST_TRIP_DETAILS:
                    presenter.loadTrips();
                    break;
            }
        }
    }

    @Override
    public void showProgress() {
        progressDialog.setMessage(getString(R.string.loading_dialog));
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }


    @Override
    public void populateTrips(List<Trip> tripList) {
        this.tripList.clear();
        if (!tripList.isEmpty()) {
            this.tripList.addAll(tripList);
            tripsListView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        } else {
            tripsListView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
        //adapter notify
        tripsListCustomAdapter.notifyDataSetChanged();
    }

    @Override
    public void showLoadTripsError() {
        Toast.makeText(getActivity(), R.string.trips_load_error, Toast.LENGTH_SHORT).show();
    }

    public static class TripItemOnClickListener implements View.OnClickListener {
        private Trip trip;
        private Fragment fragment;

        public TripItemOnClickListener(Trip trip, Fragment fragment) {
            this.trip = trip;
            this.fragment = fragment;
        }

        @Override
        public void onClick(View v) {
            showDetailScreen(trip.getId());
        }

        private void showDetailScreen(String tripID) {
            Intent intent = new Intent(fragment.getActivity(), TripDetailActivity.class);
            intent.putExtra(MainActivity.EXTRA_TRIP_ID, tripID);
            fragment.startActivityForResult(intent, TripDetailFragment.REQUEST_TRIP_DETAILS);
        }
    }
}
