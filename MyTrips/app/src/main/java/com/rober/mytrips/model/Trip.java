package com.rober.mytrips.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.rober.mytrips.data.TripPersistenceContract;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by Rober on 22/08/2016.
 */
public class Trip {
    public static SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

    @NonNull
    private String id;
    private String location;
    private String googlePlaceId;
    private Double latitude;
    private Double longitude;
    private String name;
    private Date startDate;
    private Date endDate;
    private String stayPlace;
    private String reason;
    private String companions;
    private String visitedPlaces;
    private String visitedRestaurants;
    private String comments;
    private String imagePath;


    public Trip() {
    }

    public static Trip generateTrip() {
        Trip trip = new Trip();
        trip.setId(UUID.randomUUID().toString());
        return trip;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(TripPersistenceContract.TripEntry.ID, id);
        values.put(TripPersistenceContract.TripEntry.NAME, name);
        values.put(TripPersistenceContract.TripEntry.LOCATION, location);
        values.put(TripPersistenceContract.TripEntry.GOOGLE_PLACE_ID, googlePlaceId);
        values.put(TripPersistenceContract.TripEntry.LATITUDE, latitude);
        values.put(TripPersistenceContract.TripEntry.LONGITUDE, longitude);
        values.put(TripPersistenceContract.TripEntry.START_DATE, dateFormatter.format(startDate));
        values.put(TripPersistenceContract.TripEntry.END_DATE, dateFormatter.format(endDate));
        values.put(TripPersistenceContract.TripEntry.STAY_PLACE, stayPlace);
        values.put(TripPersistenceContract.TripEntry.REASON, reason);
        values.put(TripPersistenceContract.TripEntry.COMPANIONS, companions);
        values.put(TripPersistenceContract.TripEntry.VISITED_PLACES, visitedPlaces);
        values.put(TripPersistenceContract.TripEntry.VISITED_RESTAURANTS, visitedRestaurants);
        values.put(TripPersistenceContract.TripEntry.COMMENTS, comments);
        values.put(TripPersistenceContract.TripEntry.IMAGE_PATH, imagePath);
        return values;
    }

    public static Trip readTrip(Cursor c) {
        Trip result = null;
        try {
            result = Trip.generateTrip();
            result.setId(c.getString(c.getColumnIndex(TripPersistenceContract.TripEntry.ID)));
            result.setImagePath(c.getString(c.getColumnIndex(TripPersistenceContract.TripEntry.IMAGE_PATH)));
            result.setComments(c.getString(c.getColumnIndex(TripPersistenceContract.TripEntry.COMMENTS)));
            result.setCompanions(c.getString(c.getColumnIndex(TripPersistenceContract.TripEntry.COMPANIONS)));
            result.setStartDate(dateFormatter.parse(c.getString(c.getColumnIndex(TripPersistenceContract.TripEntry.START_DATE))));
            result.setEndDate(dateFormatter.parse(c.getString(c.getColumnIndex(TripPersistenceContract.TripEntry.END_DATE))));
            result.setLocation(c.getString(c.getColumnIndex(TripPersistenceContract.TripEntry.LOCATION)));
            result.setGooglePlaceId(c.getString(c.getColumnIndex(TripPersistenceContract.TripEntry.GOOGLE_PLACE_ID)));
            if (c.getString(c.getColumnIndex(TripPersistenceContract.TripEntry.LATITUDE)) == null) {
                result.setLatitude(null);
            } else {
                result.setLatitude(c.getDouble(c.getColumnIndex(TripPersistenceContract.TripEntry.LATITUDE)));
            }
            if (c.getString(c.getColumnIndex(TripPersistenceContract.TripEntry.LONGITUDE)) == null) {
                result.setLatitude(null);
            } else {
                result.setLongitude(c.getDouble(c.getColumnIndex(TripPersistenceContract.TripEntry.LONGITUDE)));
            }
            result.setReason(c.getString(c.getColumnIndex(TripPersistenceContract.TripEntry.REASON)));
            result.setStayPlace(c.getString(c.getColumnIndex(TripPersistenceContract.TripEntry.STAY_PLACE)));
            result.setName(c.getString(c.getColumnIndex(TripPersistenceContract.TripEntry.NAME)));
            result.setVisitedPlaces(c.getString(c.getColumnIndex(TripPersistenceContract.TripEntry.VISITED_PLACES)));
            result.setVisitedRestaurants(c.getString(c.getColumnIndex(TripPersistenceContract.TripEntry.VISITED_RESTAURANTS)));

        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return result;
    }

    public String getGooglePlaceId() {
        return googlePlaceId;
    }

    public void setGooglePlaceId(String googlePlaceId) {
        this.googlePlaceId = googlePlaceId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStayPlace() {
        return stayPlace;
    }

    public void setStayPlace(String stayPlace) {
        this.stayPlace = stayPlace;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getCompanions() {
        return companions;
    }

    public void setCompanions(String companions) {
        this.companions = companions;
    }

    public String getVisitedPlaces() {
        return visitedPlaces;
    }

    public void setVisitedPlaces(String visitedPlaces) {
        this.visitedPlaces = visitedPlaces;
    }

    public String getVisitedRestaurants() {
        return visitedRestaurants;
    }

    public void setVisitedRestaurants(String visitedRestaurants) {
        this.visitedRestaurants = visitedRestaurants;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
