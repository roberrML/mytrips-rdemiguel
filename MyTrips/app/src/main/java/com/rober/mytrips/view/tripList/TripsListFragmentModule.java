package com.rober.mytrips.view.tripList;


import android.app.ProgressDialog;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.rober.mytrips.dagger.scopes.ActivityScope;
import com.rober.mytrips.interactor.trip.TripsInteractor;
import com.rober.mytrips.interactor.trip.imp.TripsInteractorImp;

import dagger.Module;
import dagger.Provides;

@Module
public class TripsListFragmentModule {

    public final TripsListContract.View view;
    public final FragmentActivity context;

    public TripsListFragmentModule(TripsListContract.View view, FragmentActivity context) {
        this.view = view;
        this.context = context;
    }

    @Provides
    @ActivityScope
    TripsListContract.View provideMainView() {
        return this.view;
    }

    @Provides
    @ActivityScope
    TripsInteractor provideTripsInteractor(TripsInteractorImp interactor) {
        return interactor;
    }

    @Provides
    @ActivityScope
    TripsListContract.Presenter provideTripsMapPresenter(TripsListPresenterImp presenter) {
        return presenter;
    }

    @Provides
    @ActivityScope
    ProgressDialog provideProgressDialog() {
        return new ProgressDialog(context);
    }

    @Provides
    @ActivityScope
    GoogleApiClient provideGoogleApiClient() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(context, 1, null)
                .build();
        return googleApiClient;
    }
}
