package com.rober.mytrips.view.emailLogin;

import android.content.Context;

import com.rober.mytrips.interactor.login.LoginInteractor;
import com.rober.mytrips.util.NetworkUtils;

import javax.inject.Inject;

/**
 * Presentador del login
 */
public class EmailLoginPresenterImp implements EmailLoginContract.Presenter {

    private Context context;
    private EmailLoginContract.View view;
    private LoginInteractor interactor;

    @Inject
    EmailLoginPresenterImp(Context context, EmailLoginContract.View view, LoginInteractor interactor) {
        this.context = context;
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void attemptLogin(String email, String password) {
        if (view != null) {
            view.showProgress();
        }
        // Check network
        if (!NetworkUtils.isNetworkAvailable(context)) {
            onNetworkConnectFailed();
        } else {
            interactor.login(email, password, new LoginInteractor.EmailLoginCallback() {
                @Override
                public void onAuthSuccess() {
                    if (view != null) {
                        view.hideProgress();
                        view.goToMainActivity();
                    }
                }

                @Override
                public void onAuthFailed(String msg) {
                    if (view != null) {
                        view.hideProgress();
                        view.showLoginError(msg);
                    }
                }

                @Override
                public void onEmailNotVerified() {
                    if (view != null) {
                        view.hideProgress();
                        view.showEmailNotVerifiedMessage();
                    }
                }
            });
        }
    }

    @Override
    public void sendVerificationEmail() {
        // Check network
        if (!NetworkUtils.isNetworkAvailable(context)) {
            onNetworkConnectFailed();
        } else {
            interactor.sendVerificationEmail();
        }

    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }


    private void onNetworkConnectFailed() {
        if (view != null) {
            view.hideProgress();
            view.showNetworkError();
        }
    }
}
