package com.rober.mytrips.dagger;

import com.rober.mytrips.dagger.modules.AppModule;
import com.rober.mytrips.dagger.modules.FirebaseModule;
import com.rober.mytrips.interactor.trip.TripsInteractorModule;
import com.rober.mytrips.view.addEditTrip.AddEditTripFragmentComponent;
import com.rober.mytrips.view.addEditTrip.AddEditTripFragmentModule;
import com.rober.mytrips.view.emailLogin.EmailLoginFragmentComponent;
import com.rober.mytrips.view.emailLogin.EmailLoginFragmentModule;
import com.rober.mytrips.view.emailRememberPassword.EmailRememberPasswordFragmentComponent;
import com.rober.mytrips.view.emailRememberPassword.EmailRememberPasswordFragmentModule;
import com.rober.mytrips.view.emailSignup.EmailSignupFragmentComponent;
import com.rober.mytrips.view.emailSignup.EmailSignupFragmentModule;
import com.rober.mytrips.view.main.MainActivityComponent;
import com.rober.mytrips.view.main.MainActivityModule;
import com.rober.mytrips.view.mainLogin.activity.MainLoginActivityComponent;
import com.rober.mytrips.view.mainLogin.activity.MainLoginActivityModule;
import com.rober.mytrips.view.mainLogin.fragment.MainLoginFragmentComponent;
import com.rober.mytrips.view.mainLogin.fragment.MainLoginFragmentModule;
import com.rober.mytrips.view.tripDetail.TripDetailFragmentComponent;
import com.rober.mytrips.view.tripDetail.TripDetailFragmentModule;
import com.rober.mytrips.view.tripList.TripsListFragmentComponent;
import com.rober.mytrips.view.tripList.TripsListFragmentModule;
import com.rober.mytrips.view.tripsMap.TripsMapFragmentComponent;
import com.rober.mytrips.view.tripsMap.TripsMapFragmentModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by roberto.demiguel on 20/12/2016.
 */
@Singleton
@Component(modules = {AppModule.class, FirebaseModule.class, TripsInteractorModule.class})
public interface AppComponent {

    EmailLoginFragmentComponent plus(EmailLoginFragmentModule module);

    EmailSignupFragmentComponent plus(EmailSignupFragmentModule module);

    EmailRememberPasswordFragmentComponent plus(EmailRememberPasswordFragmentModule module);

    MainLoginFragmentComponent plus(MainLoginFragmentModule module);

    MainLoginActivityComponent plus(MainLoginActivityModule module);

    AddEditTripFragmentComponent plus(AddEditTripFragmentModule module);

    TripDetailFragmentComponent plus(TripDetailFragmentModule module);

    TripsMapFragmentComponent plus(TripsMapFragmentModule module);

    TripsListFragmentComponent plus(TripsListFragmentModule module);

    MainActivityComponent plus(MainActivityModule module);

}
