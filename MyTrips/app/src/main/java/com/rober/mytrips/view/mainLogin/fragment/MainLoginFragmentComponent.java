package com.rober.mytrips.view.mainLogin.fragment;


import com.rober.mytrips.dagger.scopes.ActivityScope;

import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = {MainLoginFragmentModule.class})
public interface MainLoginFragmentComponent {

    void inject(MainLoginFragment fragment);
}

