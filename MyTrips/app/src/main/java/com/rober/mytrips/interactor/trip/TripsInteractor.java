package com.rober.mytrips.interactor.trip;

import android.support.annotation.NonNull;

import com.rober.mytrips.model.Trip;

import java.util.List;

/**
 * Created by roberto.demiguel on 05/01/2017.
 */
public interface TripsInteractor {

    interface LoadTripsCallback {

        void onTripsLoaded(List<Trip> trips);

        void onDataNotAvailable();
    }

    interface GetTripCallback {

        void onTripLoaded(Trip trip);

        void onDataNotAvailable();
    }

    interface TripRequestCallback {

        void onRequestSuccess();

        void onRequestFailed();
    }

    interface TripRefreshImagesCallback {

        void onFinished();
    }

    void getTrips(@NonNull TripsInteractor.LoadTripsCallback callback);

    void getTripById(@NonNull String tripId, @NonNull GetTripCallback callback);

    void saveTrip(@NonNull Trip trip, TripRequestCallback tripRequestCallback);

    void updateTrip(@NonNull Trip trip, @NonNull TripRequestCallback tripRequestCallback);

    void deleteTrip(@NonNull String tripId, @NonNull TripRequestCallback tripRequestCallback);

    void deleteAllTrips();
}
