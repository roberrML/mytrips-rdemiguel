package com.rober.mytrips.view.addEditTrip;


import com.rober.mytrips.dagger.scopes.ActivityScope;

import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = {AddEditTripFragmentModule.class})
public interface AddEditTripFragmentComponent {

    void inject(AddEditTripFragment fragment);
}

