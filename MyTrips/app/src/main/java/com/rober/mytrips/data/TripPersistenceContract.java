package com.rober.mytrips.data;

import android.provider.BaseColumns;


/**
 * Created by Rober on 22/08/2016.
 */
public class TripPersistenceContract {

    public static abstract class TripEntry implements BaseColumns {
        public static final String TABLE_NAME = "TRIPS";

        public static final String ID = "id";
        public static final String LOCATION = "location";
        public static final String GOOGLE_PLACE_ID = "googlePlaceId";
        public static final String LONGITUDE = "longitude";
        public static final String LATITUDE = "latitude";
        public static final String NAME = "name";
        public static final String START_DATE = "startDate";
        public static final String END_DATE = "endDate";
        public static final String STAY_PLACE = "stayPlace";
        public static final String REASON = "reason";
        public static final String COMPANIONS = "companions";
        public static final String VISITED_PLACES = "visitedPlaces";
        public static final String VISITED_RESTAURANTS = "visitedRestaurants";
        public static final String COMMENTS = "comments";
        public static final String IMAGE_PATH = "imagePath";
    }
}
