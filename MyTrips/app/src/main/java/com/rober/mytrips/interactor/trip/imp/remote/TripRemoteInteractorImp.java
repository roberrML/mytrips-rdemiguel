package com.rober.mytrips.interactor.trip.imp.remote;

import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rober.mytrips.interactor.exception.NoAuthenticationException;
import com.rober.mytrips.interactor.trip.TripsInteractor;
import com.rober.mytrips.model.Trip;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by roberto.demiguel on 23/01/2017.
 */
@Singleton
public class TripRemoteInteractorImp implements TripsInteractor {

    private static final String TRIPS_TABLE_NAME = "trips";
    private static final String USERS_TABLE_NAME = "users";

    private FirebaseDatabase database;
    private FirebaseAuth firebaseAuth;

    @Inject
    public TripRemoteInteractorImp(FirebaseDatabase database, FirebaseAuth firebaseAuth) {
        this.database = database;
        this.firebaseAuth = firebaseAuth;
    }

    @Override
    public void getTrips(@NonNull final LoadTripsCallback callback) {
        try {
            DatabaseReference tripsTable = getTripsDatabaseOfCurrentUser();
            final List<Trip> tripList = new ArrayList<>();
            tripsTable.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot tripSnapshot : dataSnapshot.getChildren()) {
                        Trip trip = tripSnapshot.getValue(Trip.class);
                        tripList.add(trip);
                    }
                    callback.onTripsLoaded(tripList);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    callback.onDataNotAvailable();
                }
            });
        } catch (NoAuthenticationException e) {
            callback.onDataNotAvailable();
        }

    }

    @Override
    public void getTripById(@NonNull String tripId, @NonNull final GetTripCallback callback) {
        try {
            DatabaseReference tripsTable = getTripsDatabaseOfCurrentUser();
            tripsTable.child(tripId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Trip trip = dataSnapshot.getValue(Trip.class);
                    callback.onTripLoaded(trip);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    callback.onDataNotAvailable();
                }
            });
        } catch (NoAuthenticationException e) {
            callback.onDataNotAvailable();
        }
    }

    @Override
    public void saveTrip(@NonNull Trip trip, final TripRequestCallback tripRequestCallback) {
        try {
            DatabaseReference tripsTable = getTripsDatabaseOfCurrentUser();
            tripsTable.child(trip.getId()).setValue(trip, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    tripRequestCallback.onRequestSuccess();
                }
            });
        } catch (NoAuthenticationException e) {
            tripRequestCallback.onRequestFailed();
        }

    }

    @Override
    public void updateTrip(@NonNull Trip trip, @NonNull final TripRequestCallback tripRequestCallback) {
        try {
            DatabaseReference tripsTable = getTripsDatabaseOfCurrentUser();

            tripsTable.child(trip.getId()).setValue(trip, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    tripRequestCallback.onRequestSuccess();
                }
            });
        } catch (NoAuthenticationException e) {
            tripRequestCallback.onRequestFailed();
        }
    }

    @Override
    public void deleteTrip(@NonNull String tripId, @NonNull final TripRequestCallback tripRequestCallback) {
        try {
            DatabaseReference tripsTable = getTripsDatabaseOfCurrentUser();

            tripsTable.child(tripId).removeValue(new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    tripRequestCallback.onRequestSuccess();
                }
            });
        } catch (NoAuthenticationException e) {
            tripRequestCallback.onRequestFailed();
        }

    }

    @Override
    public void deleteAllTrips() {
        try {
            DatabaseReference tripsTable = getTripsDatabaseOfCurrentUser();
            tripsTable.removeValue(new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                }
            });
        } catch (NoAuthenticationException e) {
            //TODO
        }
    }


    private DatabaseReference getTripsDatabaseOfCurrentUser() throws NoAuthenticationException {
        if (firebaseAuth.getCurrentUser() == null) {
            throw new NoAuthenticationException();
        }
        String userId = firebaseAuth.getCurrentUser().getUid();
        DatabaseReference usersTable = database.getReference(USERS_TABLE_NAME);
        DatabaseReference userRow = usersTable.child(userId);
        return userRow.child(TRIPS_TABLE_NAME);
    }
}
