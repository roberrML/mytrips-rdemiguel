package com.rober.mytrips.view.main;

import android.content.Context;

import com.rober.mytrips.interactor.RequestCallback;
import com.rober.mytrips.interactor.main.MainInteractor;
import com.rober.mytrips.interactor.trip.TripsInteractor;
import com.rober.mytrips.model.User;
import com.rober.mytrips.util.NetworkUtils;

import javax.inject.Inject;


/**
 * Created by roberto.demiguel on 11/01/2017.
 */
public class MainPresenterImp implements MainContract.Presenter {

    private Context context;
    private MainContract.View view;
    private MainInteractor mainInteractor;
    private TripsInteractor tripsInteractor;

    @Inject
    public MainPresenterImp(Context context, MainContract.View view, MainInteractor mainInteractor, TripsInteractor tripsInteractor) {
        this.context = context;
        this.view = view;
        this.mainInteractor = mainInteractor;
        this.tripsInteractor = tripsInteractor;
    }

    @Override
    public void getUserInfo() {
        // Check network
        if (!NetworkUtils.isNetworkAvailable(context)) {
            onNetworkConnectFailed();
        } else {
            mainInteractor.getUserInfo(new RequestCallback<User>() {
                @Override
                public void onRequestSuccess(User user) {
                    view.populateUserInfo(user);
                }

                @Override
                public void onRequestFailed(String msg) {
                    view.showRequestError(msg);
                }
            });
        }
    }

    @Override
    public void closeSession() {
        mainInteractor.closeSession(new RequestCallback<Object>() {
            @Override
            public void onRequestSuccess(Object object) {
                //delete trips from cache and database
                tripsInteractor.deleteAllTrips();
                view.goToMainLoginActivity();
            }

            @Override
            public void onRequestFailed(String msg) {
                view.showRequestError(msg);
            }
        });
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    private void onNetworkConnectFailed() {
        if (view != null) {
            view.showNetworkError();
        }
    }
}
