package com.rober.mytrips.view.addEditTrip;


import android.app.ProgressDialog;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Places;
import com.rober.mytrips.dagger.scopes.ActivityScope;
import com.rober.mytrips.interactor.trip.TripsInteractor;
import com.rober.mytrips.interactor.trip.imp.TripsInteractorImp;

import dagger.Module;
import dagger.Provides;

@Module
public class AddEditTripFragmentModule {

    public final AddEditTripContract.View view;
    public final FragmentActivity context;
    public final AddEditTripFragment.GoogleApiOnConnectionListener googleApiOnConnectionListener;

    public AddEditTripFragmentModule(AddEditTripContract.View view, FragmentActivity context, AddEditTripFragment.GoogleApiOnConnectionListener googleApiOnConnectionListener) {
        this.view = view;
        this.context = context;
        this.googleApiOnConnectionListener = googleApiOnConnectionListener;
    }

    @Provides
    @ActivityScope
    AddEditTripContract.View provideMainView() {
        return this.view;
    }

    @Provides
    @ActivityScope
    AddEditTripContract.Presenter providePresenter(AddEditTripPresenterImp presenter) {
        return presenter;
    }

    @Provides
    @ActivityScope
    TripsInteractor provideTripsInteractor(TripsInteractorImp interactor) {
        return interactor;
    }

    @Provides
    @ActivityScope
    GoogleApiClient provideGoogleApiClient() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(context, 0, googleApiOnConnectionListener)
                .addConnectionCallbacks(googleApiOnConnectionListener)
                .build();
        return googleApiClient;
    }

    @Provides
    @ActivityScope
    PlaceArrayAdapter providePlaceArrayAdapter() {
        AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES).build();
        return new PlaceArrayAdapter(context, android.R.layout.simple_list_item_1, null, autocompleteFilter);
    }

    @Provides
    @ActivityScope
    ProgressDialog provideProgressDialog() {
        return new ProgressDialog(context);
    }
}
