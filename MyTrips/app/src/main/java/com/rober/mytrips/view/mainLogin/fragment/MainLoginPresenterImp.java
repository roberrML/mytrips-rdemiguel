package com.rober.mytrips.view.mainLogin.fragment;

import android.content.Context;

import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.GoogleAuthProvider;
import com.rober.mytrips.interactor.RequestCallback;
import com.rober.mytrips.interactor.login.LoginInteractor;
import com.rober.mytrips.util.NetworkUtils;
import com.rober.mytrips.util.PlayServicesCheckListener;

import javax.inject.Inject;

/**
 * Created by roberto.demiguel on 05/01/2017.
 */
public class MainLoginPresenterImp implements MainLoginContract.Presenter {

    private Context context;
    private MainLoginContract.View view;
    private LoginInteractor interactor;

    @Inject
    MainLoginPresenterImp(Context context, MainLoginContract.View view, LoginInteractor interactor) {
        this.context = context;
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void handleSignInResult(GoogleSignInResult result) {
        if (view != null) {
            view.showProgress();
        }
        // Check network and google play services
        if (!NetworkUtils.isNetworkAvailable(context)) {
            onNetworkConnectFailed();
        } else if (NetworkUtils.isGooglePlayServicesAvailable(context, new PlayServicesCheckListenerImp())) {
            signIn(result);
        }
    }

    private void signIn(GoogleSignInResult result) {
        GoogleSignInAccount account = result.getSignInAccount();
        if (account == null) {
            view.hideProgress();
            view.showLoginError(null);
        } else {
            AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
            interactor.login(credential, new RequestCallback<Object>() {
                @Override
                public void onRequestSuccess(Object object) {
                    view.hideProgress();
                    view.goToMainActivity();
                }

                @Override
                public void onRequestFailed(String msg) {
                    view.hideProgress();
                    view.showLoginError(msg);
                }
            });
        }
    }


    @Override
    public void handleFacebookAccessToken(AccessToken token) {
        if (view != null) {
            view.showProgress();
        }
        // Check network and google play services
        if (!NetworkUtils.isNetworkAvailable(context)) {
            onNetworkConnectFailed();
        } else if (NetworkUtils.isGooglePlayServicesAvailable(context, new PlayServicesCheckListenerImp())) {
            AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
            interactor.login(credential, new RequestCallback<Object>() {
                @Override
                public void onRequestSuccess(Object object) {
                    view.hideProgress();
                    view.goToMainActivity();
                }

                @Override
                public void onRequestFailed(String msg) {
                    view.hideProgress();
                    view.showLoginError(msg);
                }
            });
        }
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    private void onNetworkConnectFailed() {
        if (view != null) {
            view.hideProgress();
            view.showNetworkError();
        }
    }

    private class PlayServicesCheckListenerImp implements PlayServicesCheckListener {
        @Override
        public void onBeUserResolvableError(int errorCode) {
            if (view != null) {
                view.hideProgress();
                view.showGooglePlayServicesDialog(errorCode);
            }
        }

        @Override
        public void onGooglePlayServicesFailed() {
            if (view != null) {
                view.hideProgress();
                view.showGooglePlayServicesError();
            }
        }
    }

    public interface LoginCheckListener {
        void onPreviousLogin();

        void onNotPreviusLogin();
    }

}
