package com.rober.mytrips.util;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.widget.EditText;

import com.rober.mytrips.R;

/**
 * Created by roberto.demiguel on 10/02/2017.
 */
public class FormValidatorUtil {

    public static boolean validatePassword(Context context, EditText field, TextInputLayout label) {
        boolean isValid = true;
        String password = field.getText().toString();
        if (TextUtils.isEmpty(password)) {
            label.setError(context.getString(R.string.password_empty));
            isValid = false;
        } else if (password.length() < 6) {
            label.setError(context.getString(R.string.minimum_password));
            isValid = false;
        }
        return isValid;
    }

    public static boolean validateEmail(Context context, EditText field, TextInputLayout label) {
        boolean isValid = true;
        String email = field.getText().toString();
        if (TextUtils.isEmpty(email)) {
            label.setError(context.getString(R.string.email_empty));
            isValid = false;
        } else if (!StringUtils.isEmailValid(email)) {
            label.setError(context.getString(R.string.invalid_email));
            isValid = false;
        }
        return isValid;
    }
}
