package com.rober.mytrips.view.mainLogin.fragment;

import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;

/**
 * Created by roberto.demiguel on 19/12/2016.
 */
public class MainLoginContract {

    interface View {

        void showProgress();

        void hideProgress();

        void showGooglePlayServicesDialog(int errorCode);

        void showGooglePlayServicesError();

        void showNetworkError();

        void goToMainActivity();

        void showLoginError(String msg);
    }


    interface Presenter {

        void handleSignInResult(GoogleSignInResult result);

        void handleFacebookAccessToken(AccessToken token);

        void onDestroy();
    }


}
