package com.rober.mytrips.view.addEditTrip;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.rober.mytrips.MainApplication;
import com.rober.mytrips.R;
import com.rober.mytrips.model.Trip;

import java.io.File;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddEditTripFragment extends DialogFragment implements AddEditTripContract.View {

    private static final String ARG_TRIP_ID = "arg_trip_id";
    private static final String LOG_TAG = "AddEditTripFragment";

    @BindView(R.id.name_edit_value)
    TextInputEditText nameField;
    @BindView(R.id.location_edit_value)
    AutoCompleteTextView locationAutocompleteField;
    @BindView(R.id.start_date_edit_value)
    TextView startDateField;
    @BindView(R.id.end_date_edit_value)
    TextView endDateField;
    @BindView(R.id.stay_place_edit_value)
    TextInputEditText stayPlaceField;
    @BindView(R.id.companions_edit_value)
    TextInputEditText companionsField;
    @BindView(R.id.reason_edit_value)
    TextInputEditText reasonField;
    @BindView(R.id.visited_places_edit_value)
    TextInputEditText visitedPlacesField;
    @BindView(R.id.visited_restaurants_edit_value)
    TextInputEditText visitedRestaurantsField;
    @BindView(R.id.comments_edit_value)
    TextInputEditText commentsField;
    @BindView(R.id.name_edit_label)
    TextInputLayout nameLabel;
    @BindView(R.id.location_edit_label)
    TextInputLayout locationLabel;
    @BindView(R.id.start_date_edit_label)
    TextView startDateLabel;
    @BindView(R.id.end_date_edit_label)
    TextView endDateLabel;

    @Inject
    AddEditTripContract.Presenter presenter;
    @Inject
    ProgressDialog progressDialog;
    @Inject
    GoogleApiClient googleApiClient;
    @Inject
    PlaceArrayAdapter placeArrayAdapter;

    //current trip that we are editing
    private Trip trip;
    //argument from previous activity, to show selected trip or null if we are creating
    private String tripId;
    //true if we have selected a location from autocomplete, false otherwise
    private boolean locationHasSuccessfullyChanged;

    public AddEditTripFragment() {
    }

    public static AddEditTripFragment newInstance(String tripId) {
        AddEditTripFragment fragment = new AddEditTripFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TRIP_ID, tripId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((MainApplication) getActivity().getApplication()).getAppComponent()
                .plus(new AddEditTripFragmentModule(this, getActivity(), new GoogleApiOnConnectionListener()))
                .inject(this);

        setHasOptionsMenu(true);
        //we have to set googleApiClient in presenter in order to be able to get place photos from google api
        presenter.setGoogleApiClient(googleApiClient);

        if (getArguments() != null) {
            tripId = getArguments().getString(ARG_TRIP_ID);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_edit_trip, container, false);

        ButterKnife.bind(this, root);

        locationAutocompleteField.setAdapter(placeArrayAdapter);
        locationAutocompleteField.setOnItemClickListener(autocompleteClickListener);

        locationHasSuccessfullyChanged = false;
        // Edit trip: load data
        if (tripId != null) {
            presenter.loadTrip(tripId);
        } else {
            initTrip();
        }
        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_add_edit_trip, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save) {
            saveTrip();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress() {
        progressDialog.setMessage(getString(R.string.loading_dialog));
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }


    @Override
    public void populateTrip(Trip trip) {
        this.trip = trip;
        nameField.setText(trip.getName());
        locationAutocompleteField.setText(trip.getLocation());
        startDateField.setText(Trip.dateFormatter.format(trip.getStartDate()));
        endDateField.setText(Trip.dateFormatter.format(trip.getEndDate()));
        stayPlaceField.setText(trip.getStayPlace());
        companionsField.setText(trip.getCompanions());
        reasonField.setText(trip.getReason());
        visitedPlacesField.setText(trip.getVisitedPlaces());
        visitedRestaurantsField.setText(trip.getVisitedRestaurants());
        commentsField.setText(trip.getComments());
    }

    @Override
    public void showLoadTripError() {
        Toast.makeText(getActivity(), R.string.trip_load_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showTripsScreenOnError() {
        showToastResult(R.string.trip_edit_error);
        getActivity().setResult(Activity.RESULT_CANCELED);
        getActivity().finish();
    }

    @Override
    public void showTripsScreenOnSuccess() {
        if (trip.getLongitude() == null || trip.getLatitude() == null) {
            showToastResult(R.string.trip_save_success_without_location);
        } else {
            showToastResult(R.string.trip_save_success);
        }
        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();
    }

    public class GoogleApiOnConnectionListener implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            Log.e(LOG_TAG, "Google Places API connection failed with error code: " + connectionResult.getErrorCode());
            Toast.makeText(getActivity(), getString(R.string.maps_api_connnection_failed) + connectionResult.getErrorCode(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onConnectionSuspended(int i) {
            placeArrayAdapter.setGoogleApiClient(null);
            Log.e(LOG_TAG, getString(R.string.maps_api_connection_suspended));
        }

        @Override
        public void onConnected(Bundle bundle) {
            placeArrayAdapter.setGoogleApiClient(googleApiClient);
            Log.i(LOG_TAG, "Google Places API connected.");
        }
    }


    private AdapterView.OnItemClickListener autocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = placeArrayAdapter.getItem(position);
            if (item != null) {
                final String placeId = String.valueOf(item.getPlaceId());
                Log.i(LOG_TAG, "Selected: " + item.getDescription());

                //only get new place details if place itself has changed
                if ((trip.getGooglePlaceId() == null) || !trip.getGooglePlaceId().equals(placeId)) {
                    PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(googleApiClient, placeId);
                    placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
                    Log.i(LOG_TAG, "Fetching details for ID: " + item.getPlaceId());
                }
            }
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(@NonNull PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
            } else {
                if (places.getCount() > 0) {
                    final Place place = places.get(0);
                    //set lat and long in trip to save
                    trip.setGooglePlaceId(place.getId());
                    trip.setLongitude(place.getLatLng().longitude);
                    trip.setLatitude(place.getLatLng().latitude);
                    locationHasSuccessfullyChanged = true;
                }
            }
        }
    };


    @OnClick(R.id.start_date_edit_value)
    public void onStartDateClick() {
        Calendar startDate = Calendar.getInstance();
        try {
            Date date = Trip.dateFormatter.parse(startDateField.getText().toString());
            startDate.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int year = startDate.get(Calendar.YEAR);
        int month = startDate.get(Calendar.MONTH);
        int day = startDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog startDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                startDateField.setText(getStringDate(year, monthOfYear, dayOfMonth));
            }
        }, year, month, day);
        startDatePickerDialog.show();
    }

    @OnClick(R.id.end_date_edit_value)
    public void onEndDateClick() {
        Calendar startDate = Calendar.getInstance();
        try {
            Date date = Trip.dateFormatter.parse(startDateField.getText().toString());
            startDate.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int year = startDate.get(Calendar.YEAR);
        int month = startDate.get(Calendar.MONTH);
        int day = startDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog endDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                endDateField.setText(getStringDate(year, monthOfYear, dayOfMonth));
            }
        }, year, month, day);
        endDatePickerDialog.show();
    }

    private void saveTrip() {
        if (areMandatoryFieldsFilled() && areDatesCorrect()) {
            try {
                trip.setName(nameField.getText().toString());

                String newLocation = locationAutocompleteField.getText().toString();
                //reset if we have changed location without success result
                if (!locationHasSuccessfullyChanged && !newLocation.equals(trip.getLocation())) {
                    trip.setGooglePlaceId(null);
                    trip.setLatitude(null);
                    trip.setLongitude(null);
                    trip.setImagePath(null);
                }
                //if we have changed location or set new location without photo, delete previous image file
                if ((trip.getImagePath() != null) && (locationHasSuccessfullyChanged
                        || (!locationHasSuccessfullyChanged && !newLocation.equals(trip.getLocation())))) {
                    File fileToDelete = new File(trip.getImagePath());
                    fileToDelete.delete();
                }

                trip.setLocation(newLocation);
                trip.setStartDate(Trip.dateFormatter.parse(startDateField.getText().toString()));
                trip.setEndDate(Trip.dateFormatter.parse(endDateField.getText().toString()));
                trip.setStayPlace(stayPlaceField.getText().toString());
                trip.setCompanions(companionsField.getText().toString());
                trip.setReason(reasonField.getText().toString());
                trip.setVisitedPlaces(visitedPlacesField.getText().toString());
                trip.setVisitedRestaurants(visitedRestaurantsField.getText().toString());
                trip.setComments(commentsField.getText().toString());

                //tripId is null if we are creating a trip
                if (tripId == null) {
                    presenter.saveTrip(trip);
                } else {
                    presenter.updateTrip(trip);
                }

            } catch (ParseException pe) {
                //Already checked in areDatesCorrect()
            }
        }
    }

    private boolean areDatesCorrect() {
        boolean correct = true;
        try {
            Date startDate = Trip.dateFormatter.parse(startDateField.getText().toString());
            Date endDate = Trip.dateFormatter.parse(endDateField.getText().toString());

            if (startDate.getTime() > endDate.getTime()) {
                endDateLabel.setError(getString(R.string.end_date_error));
                correct = false;
            } else {
                endDateLabel.setError(null);
            }
        } catch (ParseException e) {
            showToastResult(R.string.date_format_error);
        }

        return correct;
    }

    private boolean areMandatoryFieldsFilled() {
        String name = nameField.getText().toString();
        String location = locationAutocompleteField.getText().toString();
        String startDate = startDateField.getText().toString();
        String endDate = endDateField.getText().toString();

        return checkField(name, nameLabel)
                && checkField(location, locationLabel)
                && checkField(startDate, startDateLabel)
                && checkField(endDate, endDateLabel);
    }

    private boolean checkField(String fieldValue, TextView label) {
        boolean correct = true;
        if (TextUtils.isEmpty(fieldValue)) {
            label.setError(getString(R.string.mandatory_field_error));
            correct = false;
        } else {
            label.setError(null);
        }
        return correct;
    }

    private boolean checkField(String fieldValue, TextInputLayout label) {
        boolean correct = true;
        if (TextUtils.isEmpty(fieldValue)) {
            label.setError(getString(R.string.mandatory_field_error));
            correct = false;
        } else {
            label.setError(null);
        }
        return correct;
    }

    private void showToastResult(int resId) {
        Toast.makeText(getActivity(), resId, Toast.LENGTH_SHORT).show();
    }

    private void initTrip() {
        trip = Trip.generateTrip();
        Date today = new Date();
        Date tomorrow = new Date(today.getTime() + (1000 * 60 * 60 * 24));
        startDateField.setText(Trip.dateFormatter.format(today));
        endDateField.setText(Trip.dateFormatter.format(tomorrow));
    }

    private String getStringDate(int year, int monthOfYear, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(year, monthOfYear, dayOfMonth);
        return Trip.dateFormatter.format(c.getTime());
    }
}
