package com.rober.mytrips.util.preferencesManager;

import java.util.Map;

/**
 * Created by roberto.demiguel on 14/10/2016.
 */
public interface PreferencesManager {

    void saveStringPreference(String key, String value);

    void saveStringPreferences(Map<String, String> preferences);

    String retrieveStringPreference(String key);

    Long retrieveLastLoadFromServerPreference();

    void saveLastLoadFromServerPreference();

}
