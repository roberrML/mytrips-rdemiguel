package com.rober.mytrips.view.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.rober.mytrips.view.mainLogin.activity.MainLoginActivity;

/**
 * Created by roberto.demiguel on 26/01/2016.
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(this, MainLoginActivity.class);
        startActivity(intent);
        finish();
    }
}
