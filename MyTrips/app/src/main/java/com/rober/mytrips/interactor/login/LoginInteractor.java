package com.rober.mytrips.interactor.login;

import com.google.firebase.auth.AuthCredential;
import com.rober.mytrips.interactor.RequestCallback;
import com.rober.mytrips.view.mainLogin.fragment.MainLoginPresenterImp;

/**
 * Created by roberto.demiguel on 19/12/2016.
 */
public interface LoginInteractor {

    void login(String email, String password, final EmailLoginCallback loginCallback);

    void login(AuthCredential credential, final RequestCallback<Object> callback);

    void sendVerificationEmail();

    void signupWithEmail(String email, String password, final RequestCallback<Object> callback);

    void resetEmailPassword(String email, final RequestCallback<Object> callback);

    void checkLogin(final MainLoginPresenterImp.LoginCheckListener loginCheckListener);

    interface EmailLoginCallback {

        void onAuthFailed(String msg);

        void onEmailNotVerified();

        void onAuthSuccess();
    }
}
