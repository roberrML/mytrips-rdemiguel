package com.rober.mytrips.view.main;


import android.support.v4.app.FragmentActivity;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.rober.mytrips.R;
import com.rober.mytrips.dagger.scopes.ActivityScope;
import com.rober.mytrips.interactor.main.MainInteractor;
import com.rober.mytrips.interactor.main.imp.MainInteractorImp;
import com.rober.mytrips.interactor.trip.TripsInteractor;
import com.rober.mytrips.interactor.trip.imp.TripsInteractorImp;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {

    public final MainContract.View view;
    public final FragmentActivity context;

    public MainActivityModule(MainContract.View view, FragmentActivity context) {
        this.view = view;
        this.context = context;
    }

    @Provides
    @ActivityScope
    MainContract.View provideMainView() {
        return this.view;
    }

    @Provides
    @ActivityScope
    MainInteractor provideMainInteractor(MainInteractorImp interactor) {
        return interactor;
    }

    @Provides
    @ActivityScope
    TripsInteractor provideTripsInteractor(TripsInteractorImp interactor) {
        return interactor;
    }

    @Provides
    @ActivityScope
    MainContract.Presenter provideMainPresenter(MainPresenterImp presenter) {
        return presenter;
    }

    @Provides
    @ActivityScope
    LoginManager provideFacebookLoginManager() {
        FacebookSdk.sdkInitialize(context);
        return LoginManager.getInstance();
    }

    @Provides
    @ActivityScope
    GoogleApiClient provideGoogleApiClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(context.getString(R.string.oauth_web_client_id)).requestEmail().build();
        // Build a GoogleApiClient with access to the Google Sign-In API and the options specified by gso.
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context).enableAutoManage(context, null).addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();

        return googleApiClient;
    }
}
