package com.rober.mytrips.view.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.rober.mytrips.MainApplication;
import com.rober.mytrips.R;
import com.rober.mytrips.model.User;
import com.rober.mytrips.util.StringUtils;
import com.rober.mytrips.view.about.AboutFragment;
import com.rober.mytrips.view.mainLogin.activity.MainLoginActivity;
import com.rober.mytrips.view.tripList.TripsListFragment;
import com.rober.mytrips.view.tripsMap.TripsMapFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements MainContract.View, NavigationView.OnNavigationItemSelectedListener {

    public static final String EXTRA_TRIP_ID = "extra_trip_id";
    public static final int MAP_REQUEST_PERMISIONS_CODE = 1;
    public static final int EXTERNAL_STORAGE_REQUEST_PERMISIONS_CODE = 2;
    private static final int BACK_PRESSED_TIME_INTERVAL = 2000;
    private boolean doubleBackToExitPressedOnce = false;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    MainContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((MainApplication) getApplication()).getAppComponent().plus(new MainActivityModule(this, this)).inject(this);

        ButterKnife.bind(this);

        initUIReferences(toolbar);

        requestExternalStoragePermission();

        presenter.getUserInfo();

        //trips list by default
        if (savedInstanceState == null) {
            selectItem(R.id.nav_trips_list);
        }
    }

    private void initUIReferences(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    public void onBackPressed() {
        int backCount = getSupportFragmentManager().getBackStackEntryCount();
        if (backCount > 0) {
            super.onBackPressed();
            navigationView.setCheckedItem(R.id.nav_trips_list);
        } else {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                exitAppOnDoubleBackPressed();
            }
        }
    }

    private void exitAppOnDoubleBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
        } else {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getString(R.string.press_back_again), Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, BACK_PRESSED_TIME_INTERVAL);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        //launch new fragment
        selectItem(id);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void selectItem(int menuItemId) {
        switch (menuItemId) {
            case R.id.nav_trips_list:
                toolbar.setTitle(R.string.app_name);
                getSupportFragmentManager().beginTransaction().replace(R.id.main_content, TripsListFragment.newInstance()).commit();
                break;
            case R.id.nav_trips_map:
                toolbar.setTitle(R.string.my_trips_map);
                getSupportFragmentManager().popBackStack();
                getSupportFragmentManager().beginTransaction().replace(R.id.main_content, TripsMapFragment.newInstance()).addToBackStack("fragBack").commit();
                break;
            case R.id.nav_about:
                toolbar.setTitle(R.string.about);
                getSupportFragmentManager().popBackStack();
                getSupportFragmentManager().beginTransaction().replace(R.id.main_content, AboutFragment.newInstance()).addToBackStack("fragBack").commit();
                break;
            case R.id.nav_close_session:
                presenter.closeSession();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == MAP_REQUEST_PERMISIONS_CODE) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length != 2 || grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(MainActivity.this, "Permission denied to access your location", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == EXTERNAL_STORAGE_REQUEST_PERMISIONS_CODE) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(MainActivity.this, "Permission denied to access external storage", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void requestExternalStoragePermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    EXTERNAL_STORAGE_REQUEST_PERMISIONS_CODE);
        }
    }

    @Override
    public void showRequestError(String msg) {
        Toast.makeText(this, R.string.request_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showNetworkError() {
        Toast.makeText(this, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void populateUserInfo(User user) {
        if (navigationView.getHeaderCount() > 0) {
            View headerLayout = navigationView.getHeaderView(0);
            //name
            TextView userLabel = (TextView) headerLayout.findViewById(R.id.userName);
            userLabel.setText(StringUtils.formatString(user.getName()));
            //email
            TextView emailLabel = (TextView) headerLayout.findViewById(R.id.userEmail);
            emailLabel.setText(StringUtils.formatString(user.getEmail()));
            //avatar
            if (user.getAvatar() != null) {
                CircleImageView userImageView = (CircleImageView) headerLayout.findViewById(R.id.userAvatar);
                Glide.with(this).load(user.getAvatar()).centerCrop().into(userImageView);
            }
        }
    }

    @Override
    public void goToMainLoginActivity() {
        startActivity(new Intent(getApplicationContext(), MainLoginActivity.class));
        finish();
    }
}
