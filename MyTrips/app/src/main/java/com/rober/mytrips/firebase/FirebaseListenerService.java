package com.rober.mytrips.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.rober.mytrips.R;
import com.rober.mytrips.view.main.MainActivity;

/**
 * Created by roberto.demiguel on 23/12/2016.
 */
public class FirebaseListenerService extends FirebaseMessagingService {
    private static final String TAG = "FirebaseListenerService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, remoteMessage.getNotification().getBody());

        //Building a Notification
        NotificationCompat.Builder mBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.notific_icon)
                        .setContentTitle(getApplicationContext().getString(R.string.app_name))
                        .setAutoCancel(true)
                        .setContentText(remoteMessage.getNotification().getBody());


        //Set the Notification's Click Behavior
        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        // Gets an instance of the NotificationManager service
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // Sets an ID for the notification
        int mNotificationId = 0;
        // Builds the notification and issues it.
        notificationManager.notify(mNotificationId, mBuilder.build());

    }


}
