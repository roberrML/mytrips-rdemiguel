package com.rober.mytrips.view.emailLogin;


import com.rober.mytrips.dagger.scopes.ActivityScope;

import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = {EmailLoginFragmentModule.class})
public interface EmailLoginFragmentComponent {

    void inject(EmailLoginFragment fragment);
}

