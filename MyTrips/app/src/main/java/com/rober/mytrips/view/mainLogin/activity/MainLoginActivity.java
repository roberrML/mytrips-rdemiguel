package com.rober.mytrips.view.mainLogin.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.common.GoogleApiAvailability;
import com.rober.mytrips.MainApplication;
import com.rober.mytrips.R;
import com.rober.mytrips.view.main.MainActivity;
import com.rober.mytrips.view.mainLogin.fragment.MainLoginFragment;

import javax.inject.Inject;

public class MainLoginActivity extends AppCompatActivity implements MainLoginActivityContract.View, MainLoginFragment.GooglePlayServicesCallback {

    public static final int REQUEST_GOOGLE_PLAY_SERVICES = 1;
    public static final String FACEBOOK_PROVIDER = "facebook.com";
    public static final String EMAIL_PROVIDER = "password";
    public static final String GOOGLE_PROVIDER = "google.com";

    @Inject
    MainLoginActivityContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_login);

        ((MainApplication) getApplication()).getAppComponent().plus(new MainLoginActivityModule(this)).inject(this);

        //skip login if user is already logged
        presenter.checkLogin();

    }

    @Override
    public void goToMainActivity() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }

    @Override
    public void goToLoginActivity() {
        MainLoginFragment fragment = (MainLoginFragment) getSupportFragmentManager().findFragmentById(R.id.activity_main);
        if (fragment == null) {
            fragment = MainLoginFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.activity_main, fragment).commit();
        }
    }

    @Override
    public void onInvokeGooglePlayServices(int errorCode) {
        showPlayServicesErrorDialog(errorCode);
    }

    void showPlayServicesErrorDialog(final int errorCode) {
        Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(this, errorCode, REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

}
