package com.rober.mytrips.data.googlePlacePhoto;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.google.android.gms.location.places.PlacePhotoResult;
import com.rober.mytrips.data.ImageStorageUtil;

/**
 * Created by roberto.demiguel on 25/01/2017.
 */
public class PlacePhotoResultCallbackImp implements PlacePhotoResultCallback {

    private String googlePlaceId;
    private String packageName;
    private ObtainPhotoResultCallback callback;

    public PlacePhotoResultCallbackImp(String googlePlaceId, String packageName, ObtainPhotoResultCallback callback) {
        this.googlePlaceId = googlePlaceId;
        this.packageName = packageName;
        this.callback = callback;
    }

    @Override
    public void onResult(@NonNull PlacePhotoResult placePhotoResult) {
        if (placePhotoResult.getStatus().isSuccess()) {
            Bitmap image = placePhotoResult.getBitmap();
            String imageSavedPath = ImageStorageUtil.storeImage(image, packageName, googlePlaceId);
            callback.onPhotoSuccessfullySaved(imageSavedPath);
        } else {
            callback.onPhotoNotSaved();
        }
    }

    @Override
    public void onFailure() {
        callback.onPhotoNotSaved();
    }

}