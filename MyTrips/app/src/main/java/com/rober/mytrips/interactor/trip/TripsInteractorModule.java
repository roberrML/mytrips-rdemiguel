package com.rober.mytrips.interactor.trip;

import android.content.Context;

import com.rober.mytrips.data.TripsDbHelper;
import com.rober.mytrips.interactor.trip.imp.TripsInteractorImp;
import com.rober.mytrips.interactor.trip.imp.local.TripsLocalInteractorImp;
import com.rober.mytrips.interactor.trip.imp.remote.TripRemoteInteractorImp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by roberto.demiguel on 05/01/2017.
 */
@Module
public class TripsInteractorModule {

    @Singleton
    @Provides
    @Local
    TripsInteractor provideTasksLocalDataSource(TripsLocalInteractorImp tripsLocalInteractorImp) {
        return tripsLocalInteractorImp;
    }

    @Singleton
    @Provides
    @Remote
    TripsInteractor provideTasksRemoteDataSource(TripRemoteInteractorImp tripRemoteInteractorImp) {
        return tripRemoteInteractorImp;
    }

    @Provides
    @Singleton
    TripsDbHelper provideTripsDbHelper(Context context) {
        return new TripsDbHelper(context);
    }
}
